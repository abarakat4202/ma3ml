<?php


session_start();

if(!isset($_SESSION['labname']))
{
	header('location:index.php');
	exit();
}

else
{
	$do 		=	isset($_REQUEST['do']) ? $_REQUEST['do'] : 'manage';

	$lsidebar	=	'yes';
	$pageTitle	=	'patientsPage';

	include 'init.php';

	//
	switch ($do) {
		case 'manage':

					include $tpl.'manage-results.php';
			break;

		case 'add':

							//get tests data from database to show in select
								$stmt	=	$con->prepare(
														  "SELECT * FROM tests_templates
														   WHERE lab_ids = ? ");
								$stmt->execute(array($_SESSION['labid']));


								//fetch data in variable $row
								$rows    =	$stmt->fetchALL();
								$tests 	 = '';
								foreach ($rows as $key => $row) {

										$tests .= '<option>'.$row['test_name'].'</option>';
									}
								$price = $rows[0]['test_price'];

							$patient_id = $_REQUEST['patient_id'];		
							//get patient name		
									

							$Name   =	chkExist('patient_name','patients',"patient_id=$patient_id")[1][0][0];
							$h1		=	'Assign Test Result';
							$btn	=	'Save Result';
							$action	=	'results.php?do=assign&patient_id='.$_REQUEST['patient_id'];
							include $tpl.'test_template.php';
					break;


		case 'assign':
		
				if($_SERVER['REQUEST_METHOD'] != 'POST')
				{
					errWh(lang('noDirectAccess'));
				}
				else
				{
					$stmt	=	$con -> prepare('INSERT INTO tests_reults 
						(patient_ids,lab_ids,test_names,test_values,result_price)
						VALUES (?,?,?,?,?)');

					$stmt ->execute(array($_REQUEST['patient_id'],$_SESSION['labid'],$_REQUEST['test'],$_REQUEST['editor1'],$_REQUEST['price']));

					//get result id
    					$last_id = $con->lastInsertId();
    					$longUrl =	"http://" . $_SERVER['HTTP_HOST'].pathinfo($_SERVER['PHP_SELF'])['dirname'];
						$longUrl .= '/view.php?rsltId='.sha1($last_id);
    				//update short url	
    					$short_url	=	short($longUrl);

					$stmt	=	$con -> prepare('UPDATE tests_reults
													SET short_url = ?
													WHERE result_id = ?
													');

					$stmt ->execute(array($short_url,$last_id));
    					
    					//echo $last_id;

					//send mails and sms if activated
							 
							$url ="http://" . $_SERVER['HTTP_HOST'].pathinfo($_SERVER['PHP_SELF'])['dirname'];
							$url .= '/send.php';
				    	if(array_key_exists('auto-mail',json_decode($_SESSION['settings'],true)))
				    	{

							$postdata = http_build_query(
							    array(
							        'rsltId' => $last_id,
							        'sendType' => 'mail'
							    )
							);

							$opts = array('http' =>
							    array(
							        'method'  => 'POST',
							        'header'  => 'Content-type: application/x-www-form-urlencoded',
							        'content' => $postdata
							    )
							);

							$context  = stream_context_create($opts);

							$result = file_get_contents($url, false, $context);	    		
				   		}
				   		if(array_key_exists('auto-sms',json_decode($_SESSION['settings'],true)))
				   		{
							$postdata = http_build_query(
							    array(
							        'rsltId' => $last_id,
							        'sendType' => 'sms'
							    )
							);

							$opts = array('http' =>
							    array(
							        'method'  => 'POST',
							        'header'  => 'Content-type: application/x-www-form-urlencoded',
							        'content' => $postdata
							    )
							);

							$context  = stream_context_create($opts);

							$result = file_get_contents($url, false, $context);
				   		}
					scs();
				}
			break;

		case 'edit':

							//check if the lab elgible to edit this test
							$labId  	= $_SESSION['labid'];
							$rsltId 	= isset($_REQUEST['rsltId']) ? $_REQUEST['rsltId'] : "";
							$chkData	= chkExist('patient_ids','tests_reults','lab_ids = '."'".$labId ."'".' 
								AND result_id = '."'".$rsltId."'");
							if($chkData[0]<1)
							{	
								//NOT ELGIBLE 
								errWh(lang('noAuth'));
							}
							{

								//get tests data from database to show in select
									$stmt	=	$con->prepare(
															  "SELECT * FROM tests_templates
															   WHERE lab_ids = ? ");
									$stmt->execute(array($_SESSION['labid']));


									//fetch data in variable $row
									$testsNames    =	$stmt->fetchALL();
									$tests 	 =  ''; 

								

									$stmt	=	$con->prepare(
															  "SELECT *
															  	FROM tests_reults 
															  	INNER JOIN patients
															  	ON tests_reults.result_id = ?
															   	AND patients.patient_id = tests_reults.patient_ids ");
									$stmt->execute(array($rsltId));
									$rows = $stmt->fetchAll();

									foreach ($testsNames as $key => $testName) 
									{

										if($testName['test_name'] == $rows[0]['test_names'])
										{

											$tests .= '<option selected>'.$testName['test_name'].'</option>';
										}
										else
										{
											$tests .= '<option>'.$testName['test_name'].'</option>';
										}
									}

								//get all data 		
										
								$patient_id = $rows[0]['patient_id'];
								$Name   =	$rows[0]['patient_name'];
								$h1		=	'Edit Test Result';
								$testVal=	$rows[0]['test_values'];
								$btn	=	'Save Changes';
								$action	=	'results.php?do=update&rsltId='.$_REQUEST['rsltId'];
								include $tpl.'test_template.php';
							}
					break;


		case 'update':
		
				if($_SERVER['REQUEST_METHOD'] != 'POST')
				{
					errWh(lang('noDirectAccess'));
				}
				else
				{
					$stmt	=	$con -> prepare('UPDATE tests_reults SET 
																		test_names = ?,
																		test_values = ?
																		WHERE result_id = ?');

					$stmt ->execute(array($_REQUEST['test'],$_REQUEST['editor1'],$_REQUEST['rsltId']));
					

					scs();
				}
			break;			
			}



	//footer
	include $tpl.'footer.php';	
}