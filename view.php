<?php
include 'admin/connect.php';
	
	session_start();
	if(isset($_REQUEST['rsltId']))
	{	
		$stmt = $con->prepare('SELECT * FROM tests_reults inner JOIN patients WHERE sha1(tests_reults.result_id) = ? AND tests_reults.patient_ids = patients.patient_id');

		$stmt -> execute(array($_GET['rsltId']));
		$row = $stmt->fetchAll()[0];
		$lab_id = $row['lab_ids'];
		$body = $row['test_values'];
		$reportDate  = $row['result_date'];
		$reportName  = $row['patient_name'];
		$reportAge  = $row['patient_Age'];
		$reportSex  = $row['patient_Sex'];
		$reportReferer  = $row['referer'];
	}
	elseif (isset($_REQUEST['inv_id'])) 
	{
		$stmt = $con->prepare('SELECT * FROM invoices inner JOIN patients WHERE sha1(invoices.inovice_id) = ? AND invoices.p_id = patients.patient_id');
		$stmt -> execute(array($_GET['inv_id']));
		$row = $stmt->fetchAll()[0];
		$lab_id = $row['l_id'];
		$body = '<div class="invoice row">
					<div class="invoice-details">';
		$tests = json_decode($row['tests_names'],true);
		$amounts = json_decode($row['tests_amounts'],true);
		$dates = json_decode($row['tests_dates'],true);

		$body .= '<div class="row" style="background:grey;">';
		$body .= '<div class="col-md-4">test name</div>';
		$body .= '<div class="col-md-4">price</div>';
		$body .= '<div class="col-md-4">date</div>';
		$body .= '</div>';
		foreach ($tests as $key => $test) {
			
			$body .= '<div class="row">';
			$body .= '<div class="col-md-4">' .$test.'</div>';
			$body .= '<div class="col-md-4">' .$amounts[$key].'</div>';
			$body .= '<div class="col-md-4">' .$dates[$key].'</div>';
			$body .= '</div>';
		}

		$body .='</div>
					total : '.$row['total'].'
				</div>	';
		$reportDate  = $row['invoice_timestamp'];
		$reportName  = $row['patient_name'];
		$reportAge  = $row['patient_Age'];
		$reportSex  = $row['patient_Sex'];
		$reportReferer  = $row['referer'];
	}
	$lab_id   = $_SESSION['lab_id'];
	$noWraper = 'yes';
	include 'init.php';
	
	//get lab info
		$stmt = $con -> prepare('SELECT * FROM labs WHERE lab_id = ?');
		$stmt -> execute(array($lab_id));
		$labRow  = $stmt->fetchAll();
		$settingsArray	=json_decode($labRow[0]['settings'],true);

		$h_left 	= $labRow[0]['h_left'];
		$h_right 	= $labRow[0]['h_right'];
		$h_center 	= $labRow[0]['h_center'];
		$f_left 	= $labRow[0]['f_left'];
		$f_right 	= $labRow[0]['f_right'];
		$base = str_replace("/view.php", "/", $_SERVER['PHP_SELF']); 
		$url = isset($_SERVER['HTTPS']) ? "https" : "http" . "://$_SERVER[HTTP_HOST]".$base;
		echo '<pre>';
		print_r($url);
		echo '</pre>';
	?>

<script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>
<script src="js/html2canvas.js"></script>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div id="printableArea"  >


				      <?php include_once $tpl.'view_template.php'; ?>
		    	</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-2 col-md-offset-5 text-center">		
				<input type="button" class="btn btn-primary btn-flat" id="print" onclick="printDiv('printableArea');" value="print" />
				<input type="button" class="btn btn-primary btn-flat" id="printPDF" onclick="printPDF();" value="download" />

			</div>
		</div>	

	
<script >
function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
function printPDF() {
   html2canvas($("#printableArea"),{
   onrendered:function(canvas){

   var img=canvas.toDataURL("image/png");
   var doc = new jsPDF();
   doc.addImage(img,'JPEG',-5,0);
   doc.save('test.pdf');
   }

   });
}
</script>
<?php
	//footer
	include $tpl.'footer.php';	
?>




