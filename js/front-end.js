

$(function(){
    
    'use strict';
    
    //hide placeholder in form focus
    
    $('[placeholder]').focus(function(){
        
        $(this).attr('data-text',$(this).attr('placeholder'));
        
        $(this).attr('placeholder','');
        
    }).blur(function(){
        
        $(this).attr('placeholder',$(this).attr('data-text'));
    });


    //add astrix for required inputs
    $('input').each(function(){

        if($(this).attr('required')=='required')
        {
            $(this).after('<span class="asterisk">*</span>');
        }

    })

    //show password on eye icon hover
    $('.show-password').hover(function(){

        $('.password').attr('type','text');

    },function(){

        $('.password').attr('type','password');
    });

    // confirmation on delete user

    $('.confirm').click(function(){
        return confirm('are you sure');
    });

    // change toggle words

    var enabled = readCookie('lang') == "arabic" ? 'مفعل' : 'Enabled';
    var disabled = readCookie('lang') == "arabic" ? 'غير مفعل' : 'Disabled';

            $('.toggle').bootstrapToggle({
                  on: enabled,
                  off: disabled
                });    

    //GET DEFAULT TEMPLATE
    if($("#test").length && $('#test').is(":visible"))
    {
       getTemplate($('#test').val());
    }
    
    //add class to header 

    $('h1').addClass('page-header');
});
    //fucntion to read cookies
    function readCookie(name) 
    {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    //modal confirm

            $('#confirm').on('click', '.btn-ok', function(e) {
            var $modalDiv = $(e.delegateTarget);
            var link = $(this).data('url');
            var id   = $(this).data('id') == undefined ? "" : $(this).data('id');
            var type   = $(this).data('type') == undefined ? "" : $(this).data('type');
                    

            //$.ajax({url:link, type: 'DELETE'})

            $modalDiv.addClass('loading');

            $.post(link,{rsltId:id,sendType:type},function(data2){

            }).then(
            
            $modalDiv.modal('hide').removeClass('loading')
            )
            
            

            /*setTimeout(function() {
                ;
            }, 1000)*/
        });
        $('#confirm').on('show.bs.modal', function(e) {
            var data = $(e.relatedTarget).data();
            $('.btn-ok', this).data('url', data.url);
            try{$('.btn-ok', this).data('id', data.id);}catch(e){}
            try{$('.btn-ok', this).data('type', data.type);}catch(e){}
        });



        $(".side-nav > li > a , .side-nav > li > ul > li > ul > li > a").each(function(){
            if($(this).attr('href')==location.href.split("/").slice(-1)[0])
            {

                //li to be active 

                if($(this).parent().parent().parent().prop("tagName") == "LI" )
                {
                    var li = $(this).parent().parent().parent();
                    $(this).parent().parent().removeClass('collapse');
                    $(this).parent().parent().addClass('collapse in');

                    li.addClass('active');
                    $(this).parent().addClass('active');                    

                }
                else
                {

                    var li = $(this).parent();

                    li.addClass('active');


                }

            }
        })



    // get test template
    function getTemplate(name) 
    {
        if (name == "") {
            document.getElementById("editor1").innerHTML = "";
            return;
        } else { 
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                var xmlhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                var xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {
               
                if (this.readyState == 4 && this.status == 200) {
                    var parser = new DOMParser().parseFromString(this.responseText, "text/html");
                    // jQuery
                    $.getScript('includes/libs/ckeditor/ckeditor.js', function()
                    {
                        // script is now loaded and executed.
                        // put your dependent JS here.
    
                        //CKEDITOR.instances.editor1.destroy(true);
                        document.getElementById("editor1").innerHTML=parser.getElementById("editor1").innerHTML;
                    CKEDITOR.instances.editor1.setData(parser.getElementById("editor1").text);       
                    //CKEDITOR.instances.editor1.setMode( 'source' );    
                    //CKEDITOR.instances.editor1.setData(parser.getElementById("editor1").innerHTML);
                    //CKEDITOR.instances.editor1.setMode( 'wysiwyg', function() { alert( 'wysiwyg mode loaded!' ); } );

                    });
                        document.getElementById("price").value=parser.getElementById("price").value;

                }
            };
            xmlhttp.open("GET",'tests.php?do=edit&test='+name,true);
            xmlhttp.send();
        }
    }
    if($("#test").length && $('#test').is(":visible"))
    {
        $("#test").on('change',function(){
            getTemplate($(this).val());
        })
            
    }
