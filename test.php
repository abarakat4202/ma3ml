<?php ?>
<html>
<head>
<title>URL Shortener using Google API. http://goo.gl </title>
<script src="https://apis.google.com/js/client.js" type="text/javascript"> </script>
</head>
<script type="text/javascript">
function load() {
    gapi.client.setApiKey('AIzaSyBOd1F2hLBiDETG6NdomUEg14EIIJ736ZA');
    gapi.client.load('urlshortener', 'v1', function() { 
        document.getElementById("result").innerHTML = "";

        var Url = "http://onlineinvite.in";
        var request = gapi.client.urlshortener.url.insert({
            'resource': {
            'longUrl': Url
            }
        });
        request.execute(function(response) {

            if (response.id != null) {
                str = "<b>Long URL:</b>" + Url + "<br>";
                str += "<b>Test Short URL:</b> <a href='" + response.id + "'>" + response.id + "</a><br>";
                str += "<b>";
                document.getElementById("result").innerHTML = str;
            }
            else {
                alert("Error: creating short url \n" + response.error);
            }
        });
    });
}
window.onload = load;
</script>
<body>
<div id="result"></div>
</body>
</html>