<?php

	SESSION_START();

	/* settings page */


	if(!isset($_SESSION['labname']))
	{
		header('location:index.php');
		exit;
	}
	else
	{
		$lsidebar = 'yes';
		include 'init.php';

		if($_SERVER['REQUEST_METHOD'] === 'POST')
		{
			$stmt = $con -> prepare('UPDATE 
										labs
									SET
										settings = ?
									WHERE lab_id = ?');
			$settings = json_encode($_REQUEST);

			$stmt -> execute(array($settings,$_SESSION['labid']));

            $_SESSION['settings']     = $_REQUEST;      //store settings in session also

		}


		$stmt = $con -> prepare('SELECT * FROM labs WHERE lab_id = ?');
		$stmt -> execute(array($_SESSION['labid']));
		$row  = $stmt->fetchAll();
		$settingsArray	=json_decode($row[0]['settings'],true);

		$h_left 	= $row[0]['h_left'];
		$h_right 	= $row[0]['h_right'];
		$h_center 	= $row[0]['h_center'];
		$f_left 	= $row[0]['f_left'];
		$f_right 	= $row[0]['f_right'];


	?>

<style type="text/css">
  .demo-droppable,.demo-droppable2{
    background: #08c;
    color: #fff;
 	font-size: 40px;
    text-align: center;
  }
  .demo-droppable img,.demo-droppable2 img
  {
  	max-width: 100%;
    max-height: 100%;
  }
  .demo-droppable.dragover,.demo-droppable2.dragover{
    background: #00CC71;
  
  }
  .header
  {
  	
  	border-bottom: 2px dotted grey;
  	height: 150px!important;

  }
  .head
  {
   	/*padding: 10px 10px;*/
	overflow: hidden;
	padding: 1px!important;
	height: 100%;

  }
  .h_center
  {
  	position: relative;
	padding: 10px 10px;
  }
  .edit
  {
  	position: absolute;
	top: 10px;
    right: 0px;	
  }
  .patient_info
  {
  	height: 150px;
  	border-bottom:1px solid grey;

  }
  .temp_body
  {
  	height: 400px;
  	border-bottom:1px solid grey;
  }
  .footer
  {
  	height:150px;
  }
</style>
	<script type="text/javascript" src="<?php echo $js.'plugins/tinymce/tinymce.min.js' ?>"></script>

	<div class='container-fluid'>
		<div style="margin-top: 50px; border: 2px dotted grey; position: relative;" class="col-md-10 col-md-offset-1">
					<input type="button" class="btn btn-primary" id="clearLeft" value="clear" style="position: absolute; top: -40px; left:18px;">
					<input type="button" class="btn btn-primary" id="clearRight" value="clear" style="position: absolute; top: -40px; right:18px;">

			<div class="row header">
				<div class="col-md-3 head">

					<div class="demo-droppable head">
						<?php echo $h_left== '' ? "<p>+</p>" : "<img src=\"$h_left\">" ?>

					</div>

				</div>
				<div class="col-md-6 head-center">

					<div class="h_center" id="h_center"><?php echo $h_center== '' ? 'click to edit' : $h_center; ?></div>

				</div>
				<div class="col-md-3 head">

					<div class="demo-droppable2 head">
						<?php echo $h_right== '' ? "<p>+</p>" : "<img src=\"$h_right\">" ?>
					</div>

				</div>
			</div>
			<div class="row patient_info">

			</div>
			<div class="row temp_body">

			</div>
			<div class="row footer">
				<div class="col-md-6 footer-left">
					<div class="f_left" id="f_left"><?php echo $f_left== '' ? 'click to edit' : $f_left; ?></div>
				</div>
				<div class="col-md-6 footer-right">
					<div class="f_right" id="f_right"><?php echo $f_right=='' ? 'click to edit' : $f_right; ?></div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="col-md-10 col-md-offset-1 text-center" style="margin-top:30px;">
			<input type="submit" class="btn btn-primary" id="save" value="Save">
		</div>			
	</div>
	<script type="text/javascript">
	  (function(window) {
	    function triggerCallback(e, callback) {
	      if(!callback || typeof callback !== 'function') {
	        return;
	      }
	      var files;
	      if(e.dataTransfer) {
	        files = e.dataTransfer.files;
	      } else if(e.target) {
	        files = e.target.files;
	      }
	      callback.call(null, files);
	    }
	    function makeDroppable(ele, callback) {
	      var input = document.createElement('input');
	      input.setAttribute('type', 'file');
	      input.setAttribute('multiple', true);
	      input.style.display = 'none';
	      input.addEventListener('change', function(e) {
	        triggerCallback(e, callback);
	      });
	      ele.appendChild(input);
	      
	      ele.addEventListener('dragover', function(e) {
	        e.preventDefault();
	        e.stopPropagation();
	        ele.classList.add('dragover');
	      });

	      ele.addEventListener('dragleave', function(e) {
	        e.preventDefault();
	        e.stopPropagation();
	        ele.classList.remove('dragover');
	      });

	      ele.addEventListener('drop', function(e) {
	        e.preventDefault();
	        e.stopPropagation();
	        ele.classList.remove('dragover');
	        triggerCallback(e, callback);
	      });
	      
	      ele.addEventListener('click', function() {
	        input.value = null;
	        input.click();
	      });
	    }
	    window.makeDroppable = makeDroppable;
	  })(this);
	  (function(window) {
	    makeDroppable(window.document.querySelector('.demo-droppable'), function(files) {
	      console.log(files);
	      var output = document.querySelector('.demo-droppable');
	      output.innerHTML = '';
	      for(var i=0; i<files.length; i++) {
	        if(files[i].type.indexOf('image/') === 0) {
	          output.innerHTML += '<img src="' + URL.createObjectURL(files[i]) + '" />';
	        }
	        
	      }
	    });
	  })(this);
	    
	</script>
	<script type="text/javascript">
	  (function(window) {
	    function triggerCallback(e, callback) {
	      if(!callback || typeof callback !== 'function') {
	        return;
	      }
	      var files;
	      if(e.dataTransfer) {
	        files = e.dataTransfer.files;
	      } else if(e.target) {
	        files = e.target.files;
	      }
	      callback.call(null, files);
	    }
	    function makeDroppable(ele, callback) {
	      var input = document.createElement('input');
	      input.setAttribute('type', 'file');
	      input.setAttribute('multiple', true);
	      input.style.display = 'none';
	      input.addEventListener('change', function(e) {
	        triggerCallback(e, callback);
	      });
	      ele.appendChild(input);
	      
	      ele.addEventListener('dragover', function(e) {
	        e.preventDefault();
	        e.stopPropagation();
	        ele.classList.add('dragover');
	      });

	      ele.addEventListener('dragleave', function(e) {
	        e.preventDefault();
	        e.stopPropagation();
	        ele.classList.remove('dragover');
	      });

	      ele.addEventListener('drop', function(e) {
	        e.preventDefault();
	        e.stopPropagation();
	        ele.classList.remove('dragover');
	        triggerCallback(e, callback);
	      });
	      
	      ele.addEventListener('click', function() {
	        input.value = null;
	        input.click();
	      });
	    }
	    window.makeDroppable = makeDroppable;
	  })(this);
	  (function(window) {
	    makeDroppable(window.document.querySelector('.demo-droppable2'), function(files) {
	      console.log(files);
	      var output = document.querySelector('.demo-droppable2');
	      output.innerHTML = '';
	      for(var i=0; i<files.length; i++) {
	        if(files[i].type.indexOf('image/') === 0) {
	          output.innerHTML += '<img src="' + URL.createObjectURL(files[i]) + '" />';
	        }
	        
	      }
	    });
	  })(this);
	    
	</script>
	<script>
	$(function(){
		'use strict';

		tinymce.init({
		  selector: '.h_center',
    	  menubar:false,
		  fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",	  
		  inline: true,
		  plugins: [
		    'searchreplace visualblocks code fullscreen',
		    'paste'
		  ],
		  toolbar: ' undo redo | styleselect | sizeselect  | bold italic | fontselect |  fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		  content_css: [
		    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
		    '//www.tinymce.com/css/codepen.min.css']
		});
		tinymce.init({
		  selector: '.f_left',
    	  menubar:false,
		  fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",	  
		  inline: true,
		  plugins: [
		    'searchreplace visualblocks code fullscreen',
		    'paste'
		  ],
		  toolbar: ' undo redo | styleselect | sizeselect  | bold italic | fontselect |  fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		  content_css: [
		    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
		    '//www.tinymce.com/css/codepen.min.css']
		});

		tinymce.init({
		  selector: '.f_right',
    	  menubar:false,
		  fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",	  
		  inline: true,
		  plugins: [
		    'searchreplace visualblocks code fullscreen',
		    'paste'
		  ],
		  toolbar: ' undo redo | styleselect | sizeselect  | bold italic | fontselect |  fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
		  content_css: [
		    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
		    '//www.tinymce.com/css/codepen.min.css']
		});		$(".mce-branding-powered-by").hide();

		//store what element is edit 
		$('.edit').click(function(){
			$('#apply').attr('data-current',$(this).prev().attr('id'));
		})

		//handle modalSave btn 
		$('#apply').click(function(){
			var cur_id = $(this).attr('data-current');

			var new_val = $(this).parent().prev().find('textarea').html();
			

	        new_val = $('<textarea />').html(new_val).text();
      		$("#"+cur_id).html(new_val);
		});


		//handle click on empty div
		var editDiv = function(){
			if($(this).html().search('click to edit') >=0)
			{
				$(this).html('');
			}}
		var emptyDiv = function(){
			if($(this).html() == '')
			{
				$(this).html('click to edit');

			}

		}
		$('.f_left,.f_right,.h_center').click(editDiv).blur(emptyDiv);
		//handle submit button
		function toDataURL(url, callback) {
		  var xhr = new XMLHttpRequest();
		  xhr.onload = function() {
		    var reader = new FileReader();
		    reader.onloadend = function() {
		      callback(reader.result);
		    }
		    reader.readAsDataURL(xhr.response);
		  };
		  xhr.open('GET', url);
		  xhr.responseType = 'blob';
		  xhr.send();
		}


		$("#save").click(function(){
			var h_left_src = $(".demo-droppable").find("img").attr('src');
			var h_right_src = $(".demo-droppable2").find("img").attr('src');
			if(h_left_src !== undefined && h_left_src != "<?php echo $h_left;?>")
			{
				toDataURL(h_left_src, function(dataUrl) {
				  
					$.post( "includes/controllers/main_temp_ci.php", { h_left: dataUrl} );
				  	console.log(dataUrl)
				})

				
			}
			else if(h_left_src == undefined)
			{
					$.post( "includes/controllers/main_temp_ci.php", { h_left: ''} );

			}
			if(h_right_src !== undefined && h_right_src != "<?php echo $h_right;?>")
			{
				toDataURL(h_right_src, function(dataUrl) {
				  
					$.post( "includes/controllers/main_temp_ci.php", { h_right: dataUrl} );
				  	console.log(dataUrl)
				})

				
			}
			else if(h_right_src == undefined)
			{
					$.post( "includes/controllers/main_temp_ci.php", { h_right: ''} );

			}
					$.post( "includes/controllers/main_temp_ci.php", { h_center: $("#h_center").html(),f_left: $("#f_left").html(),f_right: $("#f_right").html()} );
						setTimeout(function(){
						   window.location.reload(1);
						}, 2000);
		});

		$("#clearLeft").click(function(){
			$(".demo-droppable").html('<p>+</p>');
		})	
		$("#clearRight").click(function(){
			$(".demo-droppable2").html('<p>+</p>');
		})	
	})
	
	</script>
	<?php
	include $tpl.'footer.php';

	}