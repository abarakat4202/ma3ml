<?php

	SESSION_START();

	/* settings page */


	if(!isset($_SESSION['labname']))
	{
		header('location:index.php');
		exit;
	}
	else
	{
		$lsidebar = 'yes';
		include 'init.php';

		if($_SERVER['REQUEST_METHOD'] === 'POST')
		{
			$stmt = $con -> prepare('UPDATE 
										labs
									SET
										settings = ?
									WHERE lab_id = ?');
			$settings = json_encode($_REQUEST);

			$stmt -> execute(array($settings,$_SESSION['labid']));

            $_SESSION['settings']     = $_REQUEST;      //store settings in session also

		}


		$stmt = $con -> prepare('SELECT settings FROM labs WHERE lab_id = ?');
		$stmt -> execute(array($_SESSION['labid']));
		$row  = $stmt->fetchAll();
		$settingsArray	=json_decode($row[0][0],true);

	?>

	<div class='container-fluid'>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h1 class="text-center"><?php echo lang('settingsTitle'); ?> </h1>
				<form method="POST" class="form-horizontal">
					<div class="form-group">
						
						<label class="col-md-2"><?php echo lang('aut-sms'); ?></label>
						<div class="col-md-6">
							<input type="checkbox" name ="auto-sms" class="toggle" 
							<?php echo isset($settingsArray['auto-sms']) ? 'checked': ''; ?>
							 data-toggle="toggle" data-onstyle="success" data-offstyle="danger">
						</div>
					</div>
					<div class="form-group">
						
						<label class="col-md-2"><?php echo lang('aut-mail'); ?></label>
						<div class="col-md-6">
							<input type="checkbox" name ="auto-mail" class="toggle" 
							<?php echo isset($settingsArray['auto-mail']) ? 'checked': ''; ?>
							 data-toggle="toggle" data-onstyle="success" data-offstyle="danger">
						</div>
					</div>
					<div class="form-group">
						
						<label class="col-md-2"><?php echo lang('sms-text'); ?></label>
						<div class="col-md-6">
						<textarea name="sms" rows=4 cols=52><?php echo isset($settingsArray['sms']) ? $settingsArray['sms']: ''; ?></textarea>
						</div>
					</div>	

					<div class="form-group">
						
						<label class="col-md-2"><?php echo lang('mail-sub'); ?></label>
						<div class="col-md-6">
						<input type="text" name="subject" class="form-control" 
						value="<?php echo isset($settingsArray['subject']) ? $settingsArray['subject'] : ''; ?>" />
						</div>
					</div>
				

					<div class="form-group">
						
						<label class="col-md-2"><?php echo lang('mail-text'); ?></label>
						<div class="col-md-10">
						<textarea id="mytextarea" name="mytextarea" required=""><?php echo isset($settingsArray['mytextarea']) ? $settingsArray['mytextarea'] : ''; ?></textarea>
						</div>
					</div>


					<div class = "form-group">
						
						<div class = "col-sm-8 col-md-6 col-sm-offset-2">
							<input type="submit" class="btn btn-primary btn-flat" value="<?php echo 'save'; ?>"  />

						</div>
					</div>						
				</form>
			</div>	
		</div>
	</div>
	<script>
$(function(){
	'use strict';
    tinymce.init({
        selector:"#mytextarea",
        branding: false,
 	 	color_picker_callback: function(callback, value) 
 	 	{
	    	callback('#FF00FF');
	  	},

        plugins: "paste lists",
        toolbar: "undo redo | bold italic underline | bullist numlist",
        menubar: false,
        browser_spellcheck: true,
        statusbar: false,
        fix_list_elements: true,
        content_style: "body.mce-content-body{ font-size: 13px; } p{ margin-top: 0.5em; margin-bottom: 0.5em; }",
        element_format : "html",
        valid_elements: "p,br,strong,bold,b,i,em,u,ul,ol,li,span[style]",
        valid_styles: {
            "span": "text-decoration"
        },
        schema: "html5",
        init_instance_callback: function(editor){
            editor.on("change", function(){
                editor.save();
                $(editor.getElement())
                .text(editor.getContent())
                .trigger("blur");

            });
            editor.on("blur", function(){
                editor.save();
                $(editor.getElement())
                .text(editor.getContent())
                .trigger("blur");
            });
        }
    });
	$(".mce-branding-powered-by").hide();
})
</script>
	<?php
	include $tpl.'footer.php';

	}