<?php

    include 'admin/connect.php';

    //ROUTES
	$tpl	=	'includes/templates/'; //templates directory
	$js		=	'js/';				   //scripts directory
	$css	=	'css/';				   //css directory
	$langs  =   'includes/languages/'; //languages directory
    $func 	= 	'includes/functions/';     //functions directory
    $libs 	= 	'includes/libs/';     //functions directory
    $img 	= 	'layout/images/';     //images directory
    $pgLen	=	10;			//items to show in 1 page
    $cPg	=	isset($_GET['p']) && intval($_GET['p']) ? $_GET['p'] : 1; //current page
    $paLimit=	5;				//pagination limit
    $lang 	=	isset($_SESSION['lang']) ? $_SESSION['lang'] : 'english';
    
    setcookie("lang",$lang ,time()+3600,"/");

	include $langs.$lang.'.php';
    include $func.'functions.php';
    
    $pageTitle = isset($pageTitle) ? lang($pageTitle) : '';

	if(isset($noWraper)){

	include $tpl.'header-no-wrapper.php';

	}
	else
	{
	include $tpl.'header.php';

	}	

	if(isset($lsidebar)){

	include $tpl.'lsidebar.php';
	}
	else if (isset($psidebar))
	{
	include $tpl.'lsidebar.php';
	}
