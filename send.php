<?php

	SESSION_START();
   	header('Content-type: text/html; charset=UTF-8');

    include 'init.php';



    if($_SERVER['REQUEST_METHOD'] == "POST")
    {
		$stmt = $con -> prepare('SELECT * FROM site_settings');
		$stmt -> execute(array());
		$row  = $stmt->fetchAll()[0];

		$unit_value 		= $row['unit_value'];
		$units_per_sms 		= $row['units_per_sms'];
		$units_per_email 	= $row['units_per_email'];

    	$stmt	=	$con -> prepare("SELECT * FROM tests_reults
    								 INNER JOIN patients
    								 INNER JOIN labs
    								 WHERE tests_reults.result_id = ? 
    								 AND patients.patient_id = tests_reults.patient_ids 
    								 AND labs.lab_id = tests_reults.lab_ids
    								 LIMIT 1 ");

    	$stmt -> execute(array($_REQUEST['rsltId']));


	    try
	    {	
	    	$row			=	$stmt->fetchAll();
	    	$row			=	$row[0];
	    	$labname		=	$row['lab_name'];
	    	$to				=	$row['email'];
	    	$toName			=	$row['patient_name'];
	    	$subject		=	isset(json_decode($row['settings'],true)['subject']) ? json_decode($row['settings'],true)['subject']: '';
			$body			=	isset(json_decode($row['settings'],true)['mytextarea']) ? json_decode($row['settings'],true)['mytextarea']: '';
			$body			.=	'<br>'.$row['test_values'].'<br>'.$row['short_url'];
			$sms			=	isset(json_decode($row['settings'],true)['sms']) ? json_decode($row['settings'],true)['sms']: '';
			$sms 			=	urlencode($sms);
			$patient_phone	=	$row['patient_phone'];
			$error = '';



	    	if($_REQUEST['sendType'] == 'mail')
	    	{
				$subAmount = $unit_value*$units_per_email;

				$credit    = chkExist('credit','labs','lab_id = '.$_SESSION['labid'])[1][0]['credit'];

				if(($credit-$subAmount)>=0)
				{	
					$stmt = $con->prepare("UPDATE labs 
											SET 
											credit = credit - ?
											WHERE lab_id = ?");
					$stmt -> execute(array($subAmount,$_SESSION['labid']));
	    			sendMail($labname,$to,$toName,$subject,$body);

				}
				else
				{
					$error = 'no credit to send email'.'<br>';
				}

	   		}
	   		else if($_REQUEST['sendType'] == 'sms')
	   		{
				$subAmount = $unit_value*$units_per_sms;

				$credit    = chkExist('credit','labs','lab_id = '.$_SESSION['labid'])[1][0]['credit'];


				if(($credit-$subAmount)>=0)
				{	
					$stmt = $con->prepare("UPDATE labs 
											SET 
											credit = credit - ?
											WHERE lab_id = ?");
					$stmt -> execute(array($subAmount,$_SESSION['labid']));
	   				sendSms($patient_phone,$sms,'ma3ml.com');
	   				print_r($sms);

				}
				else
				{
					$error .= 'no credit to send sms'.'<br>';
					errWh($error);
				}
				
	   			
	   		}




	    }catch(Notice $e)
	    {
	    	echo $e->getMessage();
	    }
    }
    else
    {
    	errWh('error');
    }
