<?php

	SESSION_START();

	/* settings page */


	if(!isset($_SESSION['username']))
	{
		header('location:index.php');
		exit;
	}
	else
	{
		$lsidebar = 'yes';
		include 'init.php';

		if($_SERVER['REQUEST_METHOD']=='POST')
		{

		$stmt = $con->prepare("UPDATE site_settings 
								SET 
								unit_value = ?,
								units_per_sms = ?,
								units_per_email = ?
								");
			$stmt -> execute(array($_REQUEST['unit'],$_REQUEST['unit-sms'],$_REQUEST['unit-email']));
		}


		$stmt = $con -> prepare('SELECT * FROM site_settings');
		$stmt -> execute(array());
		$row  = $stmt->fetchAll()[0];

	?>
	</div>
	<div class='container-fluid'>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h1 class="text-center">settings </h1>
				<form method="POST" class="form-horizontal" action="<?php echo $_SERVER['PHP_SELF']; ?> ">
		  			<div class="form-group">
						<label class= "col-sm-3 control-label">unit = </label>
						<div class="col-sm-2 col-md-1">
							<input type="text" class="form-control"
								name="unit" value="<?php echo isset($row) ? $row['unit_value'] : ""; ?>"/>
						</div>
						<label class= "col-sm-1 control-label">pound</label>
		  			</div>

		  			<div class="form-group">
						<label class= "col-sm-3 control-label">units per sms</label>
						<div class="col-sm-2 col-md-1">
							<input type="text" class="form-control"
								name="unit-sms" value="<?php echo isset($row) ? $row['units_per_sms'] : ""; ?>"/>
						</div>
		  			</div>

		  			<div class="form-group">
						<label class= "col-sm-3 control-label">units per email</label>
						<div class="col-sm-2 col-md-1">
							<input type="text" class="form-control"
								name="unit-email" value="<?php echo isset($row) ? $row['units_per_email'] : ""; ?>"/>
						</div>
		  			</div>					
		  			<div class = "form-group">
						
						<div class = "col-sm-8 col-md-6 col-sm-offset-2">
							<input type="submit" class="btn btn-primary btn-flat" value="<?php echo 'save'; ?>"  />

						</div>
					</div>						
				</form>
			</div>	
		</div>
	</div>
	
	<div>
	<?php
	include $tpl.'footer.php';

	}