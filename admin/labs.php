<?php


	/*
		=========================================
					manage members page
		=========================================


	*/

	session_start();


	//check if user logged in and if not redirect to login


	if(!isset($_SESSION['username']))
	{

		header('location:index.php');
		exit();
	}
	else
	{

		include 'init.php';
		//labs manage

		$do = isset($_GET['do']) ? $_GET['do'] : 'manage';

		if($do == 'manage')
		{


			//get labs data from database to show in manage table

			$stmt	=	$con->prepare(
									  "SELECT * FROM labs"	
				);
			$stmt->execute(array());

			//fetch data in variable $row
			$rows    =	$stmt->fetchALL();	
		?>

			<div class="container-fluid">

				<h1 class="text-center">Manage Labs</h1>
				<div class="table-responsive">

					<table class="table table-striped table-bordered table-hover text-center main-table">
						<tr>
							<td>#id</td>
							<td>name</td>
							<td>email</td>
							<td>phone</td>
							<td>credit</td>
							<td>actions</td>
						</tr>

					<?php

					foreach($rows as $row)
					{
						echo 
						'<tr>
							<td>'.$row["lab_id"].'</td>
							<td>'.$row["lab_name"].'</td>
							<td>'.$row["lab_email"].'</td>
							<td>'.$row["phone"].'</td>
							<td>'.$row["credit"].'</td>
							<td>
								<div class="btn-group">
								  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    Action <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu">
								    <li><a href="credit.php?do=edit&labid='.$row["lab_id"].'">Add Credit</a></li>
								    <li><a href="labs.php?do=edit&labid='.$row["lab_id"].'">edit</a></li>
								    <li><a href="labs.php?do=delete&labid='.$row["lab_id"].'" class="confirm">delete</a></li>
								  </ul>
								</div>


							</td>
						</tr>';
					}	
					?>
					</table>
					
				</div>

			</div>
		<?php

		}

			//add lab page

		else if($do == 'add')
		{

			//add new lab
			$h1		= 'Add lab';
			$btn	= 'Add new lab';
			$action = '?do=insert';
			include $tpl.'labmanage.php';
		}

		//SEND DATA THAT COMING FROM ADD PAGE TO DATABASE
		else if($do == 'insert')
		{
			//CHECK IF THE USER TRYING TO BROWSE THIS PAGE DIRECTLY
			if($_SERVER['REQUEST_METHOD'] !== 'POST')
			{

				$err = lang('noDirectAccess'); //error message
				redirectFun($err,1);
			}
			else
			{

				//validate the form
				$errs	=	array();

				if(empty($_REQUEST['labname']))
				{
					array_push($errs, lang('fullnameErr'));
				}
				if(empty($_REQUEST['labemail']))
				{
					array_push($errs, lang('emailErr'));
				}
				if(empty($_REQUEST['labphone']))
				{
					array_push($errs, lang('phoneErr'));
				}
				if(empty($_REQUEST['labpass']))
				{
					array_push($errs, lang('passErr'));
				}					

					if(count($errs)>0)
					{



						foreach($errs as $key => $err)
						{
							redirectFun($err,1);								
						}
					}
					else
					{					

						$stmt	=	$con->prepare('INSERT INTO labs (added_by_id,lab_name,lab_pass,lab_email,phone)
															VALUES (?,?,?,?,?)

							');
						$stmt->execute(array( $_SESSION['ID'] ,
											  $_POST['labname'] ,
											  sha1($_POST['labpass']) ,
											  $_POST['labemail'] ,
											  $_POST['labphone'] 
						 ));

						//insert completed


							scs(); // echo success message
					}
				
			}
		}

		//edit user
		else if($do == 'edit')
		{

			//check if lab selected
			if(!isset($_REQUEST['labid']))
			{
				$err = lang('noDirectAccess'); //error message
				redirectFun($err,1);
			}
			else
			{
				//edit lab
				$h1		= 'Edit Lab';
				$btn	= 'Save';
				$action = '?do=update&labid='.$_REQUEST['labid'];

				//get old data by id
				$stmt	= $con->prepare('SELECT * FROM labs  WHERE lab_id = ? LIMIT 1');
				$stmt->execute(array($_REQUEST['labid']));
				$row = $stmt->fetch();

				//set old data in the form 
				$pass = $row['lab_pass'];
				$email = $row['lab_email'];
				$phone = $row['phone'];
				$name = $row['lab_name'];

				include $tpl.'labmanage.php';
			}



		}
		//update date into database
		else if($do=='update')
		{

			//check if lab selected
			if(!(isset($_REQUEST['labid']) || $_SERVER['REQUEST_METHOD'] !== 'POST'))
			{
				$err = lang('noDirectAccess'); //error message
				redirectFun($err,1);
			}
			else
			{

					//validate the form
					$errs	=	array();

					if(empty($_REQUEST['labname']))
					{
						array_push($errs, lang('fullnameErr'));
					}
					if(empty($_REQUEST['labemail']))
					{
						array_push($errs, lang('emailErr'));
					}
					if(empty($_REQUEST['labphone']))
					{
						array_push($errs, lang('phoneErr'));
					}
				

						if(count($errs)>0)
						{



							foreach($errs as $key => $err)
							{
								redirectFun($err,1);								
							}
						}
						else{					


							$lpass 	=	isset($_POST['labpass']) ? sha1($_POST['labpass']) : $_POST['old-labpass'];
							$stmt	=	$con->prepare('UPDATE labs
														SET 
														  lab_name = ?,
														  lab_pass = ?,
														  lab_email = ?,
														  phone = ?
								');
							$stmt->execute(array( $_POST['labname'] ,
												  $lpass ,
												  $_POST['labemail'] ,
												  $_POST['labphone'] 
							 ));

							//insert completed


								scs(); // echo success message
						}
			}
		}
		else if($do=='delete')
		{
			//delete lab 

			$stmt = $con->prepare(
								"DELETE FROM labs WHERE lab_id =?"

				);
			$stmt->execute(array($_REQUEST['labid']));

			scs('the lab has been deleted');

		}

		include $tpl.'footer.php';
	}