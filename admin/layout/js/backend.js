$(function(){
    
    'use strict';
    
    //hide placeholder in form focus
    
    $('[placeholder]').focus(function(){
        
        $(this).attr('data-text',$(this).attr('placeholder'));
        
        $(this).attr('placeholder','');
        
    }).blur(function(){
        
        $(this).attr('placeholder',$(this).attr('data-text'));
    });


    //add astrix for required inputs
    $('input').each(function(){

    	if($(this).attr('required')=='required')
    	{
    		$(this).after('<span class="asterisk">*</span>');
    	}

    })

    //show password on eye icon hover
    $('.show-password').hover(function(){

    	$('.password').attr('type','text');

    },function(){

    	$('.password').attr('type','password');
    });

    // confirmation on delete user

    $('.confirm').click(function(){
        return confirm('are you sure');
    });

});
	
