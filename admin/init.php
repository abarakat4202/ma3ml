<?php
    
    include 'connect.php';

    //ROUTES
    $tpl  = 'includes/templates/';       //admin templates directory
    $css  = 'layout/css/';              //admin css directory
    $js   = 'layout/js/';              //admin js directory
    $lan  = 'includes/languages/';     //languages directory
    $func = 'includes/functions/';     //functions directory

    //include important files
	include $lan."english.php";
    include $func.'functions.php';
    include $tpl.'header.php';
    
    
    //check if the nav excepted from the page
    if(!isset($noNav))
    {
    	//include $tpl.'navbar.php';
        include $tpl.'sidebar.php';
    }





