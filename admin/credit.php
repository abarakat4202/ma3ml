<?php 

session_start();

if(isset($_SESSSION['username']))
{
   header('Location: index.php');     
   exit();
}
else
{
    include 'init.php';

	if($_SERVER['REQUEST_METHOD']=='POST')
	{
		$stmt = $con->prepare("UPDATE labs 
								SET 
								credit = credit + ?
								WHERE lab_id = ?");
		$stmt -> execute(array($_REQUEST['amount'],$_REQUEST['labid']));
		scs();
	}
	else if(isset($_GET['labid']) && is_numeric(intval($_GET['labid'])))
	{
	  ?>
	  		
	  		<div class="container-fluid">
	  			<h1 class="text-center"> add credit</h1>
		  		<form class="form-horizontal" method="post" action="credit.php?labid=<?php echo $_GET['labid']; ?> ">
		  			<div class="form-group">
						<label class= "col-sm-3 control-label">amount</label>
						<div class="col-sm-8 col-md-6">
							<input type="text" class="form-control"
								name="amount" value="<?php echo isset($name) ? $name : ""; ?>"/>
						</div>
		  			</div>
					<div class="form-group form-group-lg">

						<div class="col-sm-8 col-md-6 col-sm-offset-3">
							<input type="submit" class="btn btn-primary btn-flat" value="add" />
						</div>
					</div>		  			
		  		</form>
	  		</div>
	  		
	  <?php
	}
	else
	{
		errWh('you can\'t browse this page directly',"Error");
	}
}