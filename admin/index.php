<?php
SESSION_START();

$noNav      =   ''; 
$pageTitle  =   'Login';

if(isset($_SESSION['username']))
{
   header('Location: dashboard.php');     //redeirect to dashboard after sign in
   exit();
}

include 'init.php';
    

    //check if user coming from post request
    if($_SERVER['REQUEST_METHOD']=='POST')
    {
        $user       =       strtolower($_REQUEST['username']);
        $pass       =       $_POST['password'];
        $hashedPass =       sha1($pass);
        
        //check if user exist in database
        
        $stmt   =   $con->prepare("SELECT username,password,user_id
                                    FROM users 
                                    WHERE LCASE(username)= ?
                                    AND password= ?
                                    AND 
                                    (groupid =? OR groupid =?)
                                    LIMIT 1
                                    ");

        $stmt->execute(array($user,$hashedPass,groupid('admin'),groupid('user')));
        $row   = $stmt->fetch();
        $count = $stmt->rowCount(); 
        
        //if count > 0 that means that database contain record about this user
        if($count > 0)
        {
            echo 'welcome '.$user;
            $_SESSION['username']   = $user;                //store user in session
            $_SESSION['ID']         = $row['user_id'];      //store user id in session
            $_SESSION['groupid']    = $row['groupid'];      //store groupid in session  
            header('Location: dashboard.php');     //redeirect to dashboard after sign in

            //exit();
        }

    }
    else
    {
        
    }
?>
    
 </div> <!-- close wraper cause we don't need it here -->
    <form class="login" method="POST" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
    <h4 class='text-center' >Admin Login</h4>
        
    <div class="form-group">
        <input type="text" class="form-control input-lg" name="username"  placeholder="type your username"  />
        <i class="fa fa-user fa-fw"></i>
    </div>    
    <div class="form-group">
        <input type="password" class="form-control input-lg" name="password" placeholder="type your password" autocomplete="new-password"  />
        <i class="fa fa-lock fa-fw"></i>
    </div>    
        <input type="submit" class="btn btn-primary btn-block btn-block" name="submit" value="sign in" />


    </form>

        <div> <!-- opne tag for wraper closing in footer cause we don't need it here -->

<?php   include $tpl.'footer.php'; ?>