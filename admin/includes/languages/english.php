<?php
    

    function lang($phrase)
    {
        static $lang = array(
            

            //nav bar words
            
            'admin_home'	   => 'admin area',
            'cats'			   => 'sections',
            'edit' 			   => 'edit profile',
            'settings' 		   => 'settings',
            'logout' 		   =>  'logout',

            //dashboard
            'members'          => 'members',
            'addMember'        => 'add new member',
            'labs'             => 'labs',
            'addLab'           => 'add lab',

             //member page words

            'memberType'       =>  'membership type',
            'user'             =>  'user',
            'lab'              =>  'lab',            
            'username'         =>  'username',
            'fullname'         =>  'fullname',
            'email'            =>  'email',
            'password'         =>  'password',
            'phone'            =>  'phone',
            'noDirectAccess'   =>  'you can\'t browse this page directly',


            //edit user errs

            'usernameErr'      =>  'username must be 4 characters at least',
            'fullnameErr'      =>  'fullname can\'t be empty',
            'emailErr'         =>  'email can\'t be empty',
            'phoneErr'         =>  'phone can\'t be empty',
            'passErr'          =>  'pass can\'t be empty',

            //css filse

            'bootstrab'        => 'bootstrap.min.css',
            'sb-admin'         => 'sb-admin.css',
            'morris'           => 'plugins/morris.css',




        );
        
        return $lang[$phrase];
    }
