<?php

	/* edit and add lab temblate */

?>
				<div class="container-fluid">

				<h1 class="text-center"><?php echo isset($h1) ? $h1 : ""; ?></h1>
				<form class="form-horizontal" action="<?php echo isset($action) ? $action : ""; ?>" method="POST">
					<div class="form-group form-group-lg">

						<label class= "col-sm-3 control-label">laboratory name</label>
						<div class="col-sm-8 col-md-6">
							<!--<input type="hidden" value="<?php echo $_REQUEST['labid']?>" />-->
							<input type="text" class="form-control"
								name="labname" value="<?php echo isset($name) ? $name : ""; ?>"/>
						</div>
					</div>

					<div class="form-group form-group-lg">

						<label class= "col-sm-3 control-label">laboratory email</label>
						<div class="col-sm-8 col-md-6">
							<input type="text" class="form-control"
								name="labemail" value="<?php echo isset($email) ? $email : ""; ?>"/>
						</div>
					</div>

					<div class="form-group form-group-lg">

						<label class= "col-sm-3 control-label">laboratory password</label>
						<div class="col-sm-8 col-md-6">
							<input type="hidden" name="old-labpass" value="<?php echo isset($pass) ? $pass : ""; ?>" />
							<input type="password" class="form-control"
								name="labpass" autocomplete="new-password" />
						</div>
					</div>

					<div class="form-group form-group-lg">

						<label class= "col-sm-3 control-label">laboratory phone</label>
						<div class="col-sm-8 col-md-6">
							<input type="text" class="form-control"
								name="labphone" value="<?php echo isset($phone) ? $phone : ""; ?>"/>
						</div>
					</div>

					<div class="form-group form-group-lg">

						<div class="col-sm-8 col-md-6 col-sm-offset-3">
							<input type="submit" class="btn btn-primary btn-flat" value="<?php echo isset($btn) ? $btn : ""; ?>" />
						</div>
					</div>
										
				</form>


			</div>