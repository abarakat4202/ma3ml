<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="dashboard.php"><?php echo lang('admin_home') ?></a>
            </div>
            <!-- Top Menu Items -->
            <!--<ul class="nav navbar-nav">
                <li class="active"><a href="#">sections<span class="sr-only">(current)</span></a></li>

            </ul>-->
            <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['username'] ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="members.php?do=edit"><i class="fa fa-fw fa-user"></i><?php echo lang('edit'); ?></a>
                        </li>
                        <li>
                            <a href="settings.php"><i class="fa fa-fw fa-gear"></i><?php echo lang('settings'); ?></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i><?php echo lang('logout'); ?></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="dashboard.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-users"></i><?php echo lang('members'); ?><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="members.php?do=add&type=user"><?php echo lang('addMember');?></a>
                            </li>
                            <li>
                                <a href="members.php">members list</a>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#labs"><i class="fa fa-fw fa-users"></i><?php echo lang('labs'); ?><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="labs" class="collapse">
                            <li>
                                <a href="labs.php?do=add"><?php echo lang('addLab');?></a>
                            </li>
                            <li>
                                <a href="labs.php">labs list</a>
                            </li>
                        </ul>
                    </li>                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        
    