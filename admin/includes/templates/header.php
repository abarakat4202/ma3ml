<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo pTitle(); ?></title>
        <link rel="stylesheet" href="<?php echo $css.lang('bootstrab'); ?>">
        <link rel="stylesheet" href="<?php echo $css.lang('sb-admin'); ?>">
        <link rel="stylesheet" href="<?php echo $css.lang('morris'); ?>">

        <link rel="stylesheet" href="<?php echo $css; ?>font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $css; ?>backend.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700,900" rel="stylesheet">        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->        
    </head>
    
    <body>

         <div id="wrapper">

    