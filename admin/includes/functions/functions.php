<?php

	/*
		
		function that echo title if the variable $pageTitle is not empty

	*/
		function pTitle()
		{
			global $pageTitle;

			if(isset($pageTitle))
			{
				return $pageTitle;
			}
			else
			{
				return 'Title';
			}
		}

	/*
		get groupid of authrized members
	*/
		function groupId($type)
		{
			$ids	=	array('admin','user','lab','patient');

			return array_search(strtolower($type),$ids);
		}

		//redirect func

		function redirectFun($err,$seconds = 3 , $redirectTo='index.php'){

			echo '<div class="container-fluid">
								<div class="row">
									<div class="col-md-6 col-md-offset-3" style="margin-top:5px;">';
			echo "<div class=\"alert alert-danger text-center\">$err</div></div></div></div>";

        /*flush(); // Flush the buffer
        ob_flush();

			header("refresh:$seconds;url=$redirectTo");

			exit();*/
		}
		////////////////////////////////
		///// error msg with header ////
		///////////////////////////////

		function errWh($err,$h1=""){

			echo '<div class="container-fluid">
								<h1 class="text-center">'.$h1.'</h1>
								<div class="row">
									<div class="col-md-6 col-md-offset-3" style="margin-top:5px;">';
			echo "<div class=\"alert alert-danger text-center\">$err</div></div></div></div>";

		}
		/////////////////////////////
		///// success msg  //////////
		////////////////////////////

		function scs($msg= 'your updates has been saved'){
						echo '
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-6 col-md-offset-3" style="margin-top:5px;">
									<div class="alert alert-success text-center" role="alert">';								
						echo $msg;

						echo			'</div>
								</div>
							</div>
						</div>';
		}