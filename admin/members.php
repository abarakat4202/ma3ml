<?php
	
		/*
			=========================================
						manage members page
			=========================================


		*/

	session_start();

	$pageTitle = 'members';

	if(isset($_SESSION['username']))
	{
		include 'init.php';

		//check the operation
		$do 	=	isset($_REQUEST['do']) ? $_REQUEST['do'] : 'manage';

		//handle the operation
		if($do == 'manage')
		{	//start manage page

			//get users data from database to show in manage table

			$stmt	=	$con->prepare(
									  "SELECT * FROM users WHERE groupid=0 OR groupid=1"	
				);
			$stmt->execute(array());

			//fetch data in variable $row
			$rows    =	$stmt->fetchALL();	
		?>
			
			<div class="container-fluid">

				<h1 class="text-center">Manage Members</h1>
				<div class="table-responsive">

					<table class="table table-striped table-bordered table-hover text-center main-table">
						<tr>
							<td>#id</td>
							<td>full name</td>
							<td>username</td>
							<td>phone</td>
							<td>email</td>
							<td>actions</td>
						</tr>

					<?php

					foreach($rows as $row)
					{
						echo 
						'<tr>
							<td>'.$row["user_id"].'</td>
							<td>'.$row["fullname"].'</td>
							<td>'.$row["username"].'</td>
							<td>'.$row["phone"].'</td>
							<td>'.$row["email"].'</td>
							<td>
								<div class="btn-group">
								  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								    Action <span class="caret"></span>
								  </button>
								  <ul class="dropdown-menu">
								    <li><a href="members.php?do=edit&id='.$row["user_id"].'">edit</a></li>
								    <li><a href="members.php?do=delete&id='.$row["user_id"].'" class="confirm">delete</a></li>
								  </ul>
								</div>


							</td>
						</tr>';
					}	
					?>
					</table>
					
				</div>

			</div>
		<?php
		}

		else if($do == 'add')


		{
			//ADD MEMBER PAGE
			?>
			
			

			<div class="container-fluid">
				<h1 class="text-center">Add Member</h1>
				<form class='form-horizontal' action='?do=insert' method='post'>

					<div class='form-group form-group-lg'>
						
					<input type='hidden' name='typeValue' value="<?php echo $_GET['type']; ?> "/>
					</div>

					<div class='form-group form-group-lg'>
						
						<label class='col-sm-2 control-label'><?php echo lang('username')?></label>
						<div class="col-sm-8 col-md-6">

							<input type='text' class='form-control'
							name='username' required="required"/>
						</div>	
					</div>
					<div class='form-group form-group-lg'>
						
						<label class='col-sm-2 control-label'><?php echo lang('fullname') ?></label>
						<div class="col-sm-8 col-md-6">
							<input type='text' class='form-control'
							name='fullname'	required="required"/>
						</div>	
					</div>
					<div class='form-group form-group-lg'>
						
						<label class='col-sm-2 control-label'><?php echo lang('email')?></label>
						<div class="col-sm-8 col-md-6">
							<input type='email' class='form-control'
							name='email' required="required"/>
						</div>	
					</div>
					<div class='form-group form-group-lg'>
						
						<label class='col-sm-2 control-label'  ><?php echo lang('password')?></label>
						<div class="col-sm-8 col-md-6">
							<input type='password' class='form-control password'
							 name='password'	autocomplete='new-password' 
							 required="required" />
							 <i class='fa fa-eye show-password'></i>

						</div>

					</div>

					<div class='form-group form-group-lg'>
						
						<label class='col-sm-2 control-label'><?php echo lang('phone')?></label>
						<div class="col-sm-8 col-md-6">
							<input type='number' class='form-control'
							 name='phone'	value="<?php echo $row['phone']; ?>"
							 required="required"/>
						</div>	
					</div>
					<div class='form-group form-group-lg'>
						
						<div class="col-sm-8 col-sm-offset-2 ">
							<input type='submit' class='btn btn-primary btn-lg btn-flat' value='Add Member'	/>
						</div>	
					</div>																				
				</form>

			</div>
		<?php
		}

		else if($do == 'edit')
		{
			//start edit page

			$user_id = isset($_REQUEST['id']) ? $_REQUEST['id'] : $_SESSION['ID'];


			$stmt 	= $con->prepare("SELECT * From users WHERE user_id = ? LIMIT 1");
			$stmt->execute(array($user_id));
			$row	= $stmt->fetch();

			?>

			<div class="container-fluid">
				<h1 class="text-center">Edit User</h1>
				<form class='form-horizontal' action='?do=update' method='post'>
					<div class='form-group form-group-lg'>
						
						<label class='col-sm-2 control-label'><?php echo lang('username')?></label>
						<div class="col-sm-8 col-md-6">
							<input type="hidden" name='userid' value="<?php echo $user_id; ?>"/>
							<input type='text' class='form-control'
							name='username' value="<?php echo $row['username']; ?>"
							required="required"/>
						</div>	
					</div>
					<div class='form-group form-group-lg'>
						
						<label class='col-sm-2 control-label'><?php echo lang('fullname') ?></label>
						<div class="col-sm-8 col-md-6">
							<input type='text' class='form-control'
							name='fullname'	value="<?php echo $row['fullname']; ?>"required="required"/>
						</div>	
					</div>
					<div class='form-group form-group-lg'>
						
						<label class='col-sm-2 control-label'><?php echo lang('email')?></label>
						<div class="col-sm-8 col-md-6">
							<input type='email' class='form-control'
							name='email' value="<?php echo $row['email']; ?>"
							required="required"/>
						</div>	
					</div>
					<div class='form-group form-group-lg'>
						
						<label class='col-sm-2 control-label'  ><?php echo lang('password')?></label>
						<div class="col-sm-8 col-md-6">
							<input type='hidden' class='form-control' name='oldPassword' value="<?php echo $row['password']; ?>"autocomplete='new-password'  />
							<input type='password' class='form-control password'
							 name='password' autocomplete='new-password'
							 placeholder = "leave it blank if you don't want to change it"  />
							 <i class='fa fa-eye'></i>
						</div>	
					</div>
					<div class='form-group form-group-lg'>
						
						<label class='col-sm-2 control-label'><?php echo lang('phone')?></label>
						<div class="col-sm-8 col-md-6">
							<input type='number' class='form-control'
							 name='phone'	value="<?php echo $row['phone']; ?>"
							 required="required"/>
						</div>	
					</div>
					<div class='form-group form-group-lg'>
						
						<div class="col-sm-8 col-sm-offset-2 ">
							<input type='submit' class='btn btn-primary btn-lg btn-flat' value='save'	/>
						</div>	
					</div>																				
				</form>

			</div>
		<?php 
		}

			
		
		else if($do == 'update')
		{
			//check if user redirected from edit page with post method or not
			if($_SERVER['REQUEST_METHOD'] != 'POST')
			{

				//came directly without post method
				echo lang('noDirectAccess');
			}
			else
			{
				//start update page
				//page header
				echo '<h1 class="text-center">Update Member</h1>';
				//validate the form
					$errs	=	array();
					if(strlen($_REQUEST['username'])<4)
					{
						array_push($errs, lang('usernameErr'));
					}
					if(empty($_REQUEST['fullname']))
					{
						array_push($errs, lang('fullnameErr'));
					}
				
					if(empty($_REQUEST['email']))
					{
						array_push($errs, lang('emailErr'));
					}
					if(empty($_REQUEST['phone']))
					{
						array_push($errs, lang('phoneErr'));
					}

						if(count($errs))
						{



							foreach($errs as $key => $err)
							{
								echo '
								<div class="container">
									<div class="row">
										<div class="col-md-6 col-md-offset-3" style="margin-top:5px;">
											<div class="alert alert-danger text-center" role="alert">';								
								echo $err;

								echo			'</div>
										</div>
									</div>
								</div>';								
							}


						}
						else
						{															
							//update datebase with new data if no errs

							$pass   =	empty($_REQUEST['password']) ? $_REQUEST['password'] : sha1($_REQUEST['oldPassword']);
							$stmt	=	$con->prepare("UPDATE 
														users 
													SET
													 username = ?,
													 fullname = ?,
													 email = ?,
													 password = ?,
													 phone = ?

													WHERE user_id = ?"								 
													  );
							$stmt->execute(array(
													$_REQUEST['username'],
													$_REQUEST['fullname'],
													$_REQUEST['email'],
													$pass,
													$_REQUEST['phone'],
													$_REQUEST['userid']

												));

							//update completed
								echo '
								<div class="container-fluid">
									
										<div class="col-md-6 col-md-offset-3" style="margin-top:5px;">
											<div class="alert alert-success text-center" role="alert">';								
								echo "your updates has been saved";

								echo			'</div>
										</div>
									
								</div>';

							//redirect after 3 seconds
								//sleep(3);
								//header('location:dashboard.php');
							
						}
					

			}
		}
		else if($do == 'insert')
		{	
			//check if user comes with post method

			if($_SERVER['REQUEST_METHOD'] !== 'POST')
			{
				redirectFun('ERROR',3 , 'index.php');
				echo '<h4 class="text-center">'.lang('noDirectAccess').'</h1>';

				

				
			}
			else
			{	
				//start insert page
				//page header
				echo '<h1 class="text-center">Add Member</h1>';
				//validate the form
				$errs	=	array();
				if(strlen($_REQUEST['username'])<4)
				{
					array_push($errs, lang('usernameErr'));
				}
				if(empty($_REQUEST['fullname']))
				{
					array_push($errs, lang('fullnameErr'));
				}
				if(empty($_REQUEST['email']))
				{
					array_push($errs, lang('emailErr'));
				}
				if(empty($_REQUEST['phone']))
				{
					array_push($errs, lang('phoneErr'));
				}
				if(empty($_REQUEST['password']))
				{
					array_push($errs, lang('passErr'));
				}					

					if(count($errs)>0)
					{



						foreach($errs as $key => $err)
						{
							echo '
							<div class="container">
								<div class="row">
									<div class="col-md-6 col-md-offset-3" style="margin-top:5px;">
										<div class="alert alert-danger text-center" role="alert">';								
							echo $err;

							echo			'</div>
									</div>
								</div>
							</div>';								
						}


					}
					else
					{															
						//insert datebase with new data if no errs

						$stmt	=	$con->prepare("
									INSERT INTO users 
									(username,fullname,email,password,phone,groupid)
									VALUES
									(?,?,?,?,?,?)
							");
						$stmt->execute(array($_REQUEST['username'],
											 $_REQUEST['fullname'],
											 $_REQUEST['email'],
											 sha1($_REQUEST['password']),
											 $_REQUEST['phone'],
											 groupId(trim($_REQUEST['typeValue']," +")),
							));

						//insert completed
						echo '
						<div class="container">
							<div class="row">
								<div class="col-md-6 col-md-offset-3" style="margin-top:5px;">
									<div class="alert alert-success text-center" role="alert">';								
						echo "your updates has been saved ";

						echo			'</div>
								</div>
							</div>
						</div>';

						//redirect after 3 seconds
						//sleep(3);
						//header('location:dashboard.php');
					}
			}
		}
		else if($do == 'delete'){

			//delete member 

			$stmt = $con->prepare(
								"DELETE FROM users WHERE user_id =?"

				);
			$stmt->execute(array($_GET['id']));

								echo '
								<div class="container-fluid">
									<h1 class="text-center">delete member</h1>
									
										<div class="col-md-6 col-md-offset-3" style="margin-top:5px;">
											<div class="alert alert-success text-center" role="alert">';								
								echo "user has been deleted";

								echo			'</div>
										</div>
									
								</div>';


		}

		include $tpl.'footer.php';
	}
	else
	{
		header('location:index.php');
		exit();
	}