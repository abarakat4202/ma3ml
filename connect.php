<?php
    
    $server     =   'localhost';
    $dbnName    =   'lab';
    $dsn        =   'mysql:host='.$server.';dbname='.$dbnName;
    $user       =   'root';
    $pass       =   '';
    $opt        =   array(
            
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
    );

    try
    {
        
        $con = new PDO($dsn,$user,$pass,$opt);
        
        $con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        
        //echo 'you are connected';
        //$conn
    }
    catch(PDOException $e)
    {
        echo 'FAILED TO CONNECT : '.$e->getMessage();
    }