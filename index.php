<?php
    
    session_start();

    $noNav      =   'yes';
    $pageTitle  =   'Login';
    if(isset($_SESSION['labname']))
    {
        header('Location:ldashboard.php');     //redeirect to labs dashboard after sign in
        exit();
    }


    include 'init.php';

    //check if coming from post request
    if($_SERVER['REQUEST_METHOD']=='POST')
    {
        $lab        =       strtolower($_REQUEST['labname']);
        $pass       =       $_POST['password'];
        $hashedPass =       sha1($pass);
        
        //check if user exist in database
        
        $stmt   =   $con->prepare("SELECT *
                                    FROM labs 
                                    WHERE LCASE(lab_name)= ?
                                    AND lab_pass= ?
                                    LIMIT 1
                                    ");

        $stmt->execute(array($lab,$hashedPass));
        $row   = $stmt->fetch();
        $count = $stmt->rowCount(); 
        
        //if count > 0 that means that database contain record about this user
        if($count > 0)
        {
            echo 'welcome '.ucfirst($lab);
            $_SESSION['labname']   = $lab;                //store user in session
            $_SESSION['labid']     = $row['lab_id'];      //store user id in session
            $_SESSION['lab_id']     = $row['lab_id'];      //store user id in session
            $_SESSION['settings']  = $row['settings'];      //store settings in session also
            header('Location:ldashboard.php');     //redeirect to dashboard after sign in

            exit();
        }
        else
        {
            //echo credentials error
        }

    }
    else
    {
        
    }

    ?>

        </div> <!-- close wraper cause we don't need it here -->
        <div class="container text-center">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">

                        <form class="login-form" action="<?php echo $_SERVER['PHP_SELF']; ?> " method="POST" >

                            <div class="form-group">
                                <input type="text" name="labname" class="form-control" placeholder="type your username" />
                                <i class="fa fa-user fa-fw"></i>
                            </div>    
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="type your password"  />
                                <i class="fa fa-lock fa-fw"></i>
                            </div>    
                                <input type="submit" class="btn btn-primary btn-block btn-flat" name="submit" value="sign in" />
                            
                            
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div> <!-- opne tag for wraper closing in footer cause we don't need it here -->
        <?php
            include $tpl.'footer.php';
            ?>