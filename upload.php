<?php

SESSION_START();
//print_r($_FILES);
//json_encode($_FILES);
$target_dir =  "data/uploads/images/";
//$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

$target_file = $target_dir.$_SESSION['labid'].'_header.'.pathinfo($_FILES["upload"]["name"],PATHINFO_EXTENSION);

$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["upload"]["tmp_name"]);
    if($check !== false) {
        //echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
      //  echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
/*if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}*/
// Check file size
if ($_FILES["upload"]["size"] > 500000) {
   // echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    //echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["upload"]["tmp_name"], $target_file)) {
        //echo "The file ". basename( $_FILES["upload"]["name"]). " has been uploaded.";
    } else {
        //echo "Sorry, there was an error uploading your file.";
    }
}


?>

<?php


json_encode($_FILES);

$res = array("uploaded" 	=> $uploadOk,
		    	"url" 		=> $target_file,
			    "error"		=> array(
	        	"message" 	=> "error"
					    )
					);
echo json_encode($res);

?>
