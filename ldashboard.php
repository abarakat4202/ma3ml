<?php
SESSION_START();

	/*

		=====================
		== labs dashboard ===
		=====================


	*/


if(isset($_SESSION['labname']))
{

	$lsidebar	=	''; 					//to set lab sidebar
	$pageTitle	=	'ldashboard';		//to set page title from language file
	//intialize includes
	include 'init.php';

	$patientsCnt	=	count(chkExist("*",'labs_patients_relation_table','lab_ids='.$_SESSION['labid'])[1]);
	$reportsCnt		=	count(chkExist("*",'tests_reults','lab_ids='.$_SESSION['labid'])[1]);
	$tplCnt			=	count(chkExist("*",'tests_templates','lab_ids='.$_SESSION['labid'])[1]);
	
	//include template
	include $tpl.'dashboard-template.php';

	//end - include footer
   include $tpl.'footer.php';
}
else
{
	header('Location: index.php');

	exit();

}