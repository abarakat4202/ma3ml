<?php


session_start();

if(!isset($_SESSION['labname']))
{
	header('location:index.php');
	exit();
}

else
{
	$do 		=	isset($_REQUEST['do']) ? $_REQUEST['do'] : 'manage';

	$lsidebar	=	'yes';
	$pageTitle	=	'patientsPage';

	include 'init.php';

	//
	switch ($do) {
		case 'manage':

					include $tpl.'manage-invoices.php';
			break;
		case 'order':
					//get tests data from database to show in manage table
					$stmt	=	$con->prepare(
											  "SELECT * FROM tests_templates
											   WHERE lab_ids = ?");
					$stmt->execute(array($_SESSION['labid']));


					//fetch data in variable $row
					$rows    =	$stmt->fetchALL();

					$h1 = 'choose tests to add to invoice';
					$btn = 'order';

					include $tpl.'order.php';
			break;


		case 'assign':

				//check if 	comes with valid patient id and under this lab proberty 

		
				if($_SERVER['REQUEST_METHOD'] != 'POST' &&  chkExist("*",'labs_patients_relation_table',
						'patient_ids	 = ' . $_REQUEST['patient_id'] . ' AND lab_ids	= ' . $_SESSION['labid'] )[0] <= 0)
				{
					errWh(lang('noDirectAccess'));
				}
				else
				{
					$stmt	=	$con -> prepare('INSERT INTO invoices 
						(p_id,l_id,tests_names,tests_amounts,tests_dates,total)
						VALUES (?,?,?,?,?,?)');

					$stmt ->execute(array($_REQUEST['patient_id'],$_SESSION['labid'],json_encode($_REQUEST['tests_names']),json_encode($_REQUEST['tests_amouts']),json_encode($_REQUEST['tests_dates']),$_REQUEST['total']));


					scs();
				}
			break;
			}



	//footer
	include $tpl.'footer.php';	
}