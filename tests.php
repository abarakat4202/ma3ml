<?php

	session_start();


	/*

		=====================
		== tests dashboard ===
		=====================


	*/


if(!isset($_SESSION['labname']))
{
	header('Location: index.php');

	exit();

}
else
{

	$do 	= isset($_GET['do']) ? $_GET['do'] : 'manage';

	$lsidebar	=	'yes'; 					//to set lab sidebar
	$pageTitle	=	'Tests';		//to set page title from language file

	//intialize includes
	include 'init.php';

	switch ($do) 
	{
		case 'manage':


			include $tpl.'manage-tests.php';

			break;

		case 'add':

			$action = '?do=insert';
			$btn	=	lang('save');
			$h1		=	lang('addTemp');
			include $tpl.'test_template.php';

			break;

		case 'insert':

			if($_SERVER['REQUEST_METHOD']!=='POST')
			{
				redirectFun('you can\'t browse this page directly');
			}
			else
			{
				// check if test name is exist with this lab and if yes echo error if not add the test

				$Name 	= strtolower($_REQUEST['name']);
				$test_value	=	urldecode($_REQUEST['editor1']);


				$stmt = $con->prepare("SELECT test_name
										 FROM tests_templates
										 WHERE LCASE(test_name) = ?
										 AND lab_ids = ?");

				$stmt -> execute(array($Name,$_SESSION['labid']));

				$count = $stmt->rowCount();

				if($count > 0)
				{
					errWh(lang('dublicateTemp'));
				}
				else
				{
					$stmt = $con -> prepare("INSERT INTO 
						tests_templates (lab_ids,test_name,test_price,test_value)
						VALUES (?,?,?,?)");

					$stmt -> execute(array($_SESSION['labid'],$_REQUEST['name'],$_REQUEST['price'],$test_value));

					scs();
				}


			}

		break;
		
		case 'edit':

			if(isset($_REQUEST['test']))
			{

				$Name 	=	strtolower($_REQUEST['test']);
				$data = chkExist("*",'tests_templates',"LCASE(test_name)='$Name' 
					AND lab_ids='".$_SESSION['labid']."'");
				if($data[0] > 0 )
				{

				$Name 	=	$_REQUEST['test'];
				$testVal 	=	$data[1][0]['test_value'];
				$price 	=	$data[1][0]['test_price'];

				$h1		=	lang('editTemp');
				$action = '?do=update';
				$btn	=	lang('save');
				include $tpl.'test_template.php';
				}
				else
				{
					errWh(lang('noTest'));
				}


			}

			break;

		case 'update':

			if($_SERVER['REQUEST_METHOD']!=='POST')
			{
				redirectFun('you can\'t browse this page directly');
			}
			else
			{
				// check if test name is exist with this lab and if yes echo error if not add the test

				$Name 	= strtolower($_REQUEST['name']);
				$test_value	=	urldecode($_REQUEST['editor1']);


				$stmt = $con->prepare("SELECT test_name
										 FROM tests_templates
										 WHERE LCASE(test_name) = ?
										 AND lab_ids = ?");

				$stmt -> execute(array($Name,$_SESSION['labid']));

				$count = $stmt->rowCount();

				if($count < 1)
				{
					errWh('no test template with this name');
				}
				else
				{
					$stmt = $con -> prepare("UPDATE tests_templates
												SET test_value = ?,
												test_price = ?
												 WHERE LCASE(test_name) = ?
												 AND lab_ids = ?");


					$stmt -> execute(array($test_value,$_REQUEST['price'],$_REQUEST['name'],$_SESSION['labid']));
					echo("<script>location.href = 'tests.php';</script>");
					scs();
				}


			}

		break;

		default:
		;
			break;
	}


	//end - include footer
   include $tpl.'footer.php';

}
