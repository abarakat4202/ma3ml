<?php
      
      
	function lang($phrase)
	{

		$phrases = array(

		//pages titles
		'defaultTitle'	   => 'المعمل',
		'ldashboard'	   => 'الرئيسية',
		'Login'	   	   	   => 'تسجيل الدخول',
            'patientsPage'     => 'المرضى',
            'Tests'            => 'التحاليل',
            //nav bar words
            
            'admin_home'	   => 'admin area',
            'cats'	           => 'sections',
            'editProfile' 	   => 'تعديل',
            'settings' 		   => 'الإعدادات',
            'logout' 		   =>  'تسجيل الخروج',

            //dashboard
            'members'          => 'members',
            'addMember'        => 'add new member',
            'labs'             => 'labs',
            'dashboard'        => 'الرئيسية',
            'patients'         => 'المرضى',
            'addPatient'       => 'إضافة مريض',
            'patientList'      => 'قائمة المرضى',
            'reports'          => 'التقارير',
            'templates'        => 'النماذج',
            'addTemp'          => 'إضافة نموذج',
            'tempList'         => 'قائمة النماذج',

             //patient page words

            'memberType'       =>  'membership type',
            'user'             =>  'user',
            'lab'              =>  'lab',            
            'username'         =>  'username',
            'fullname'         =>  'fullname',
            'pEmail'           =>  'الإيميل',
            'password'         =>  'كلمة السر',
            'noDirectAccess'   =>  'غير مسموح بتصفح هذ الصفحة',
            'pName'            =>   'الإسم',
            'pPhone'           =>   'الهاتف',
            'pId'              =>   'رقم البطاقة',
            'pAge'             =>   'السن',
            'pSex'             =>   'الجنس',
            'actions'          =>   'actions',
            'male'			   =>	'ذكر',
            'female'		   =>	'أنثى',
            
            //actions
            
            'edit'  		=>  'تعديل',
            'assign'  		=>  'إضافة تقرير',
            
            //manage results
            'rPname'    	=> 'إسم المريض',
            'rTname'    	=> 'إسم النموذج',
            'date'    		=> 'التاريخ',
            'noAuth'		=>	'غير مسموح لك بتصفح هذا التقرير',
            'addReport'			=>	'إضافة تقرير',
            
            //manage tests
            'manageTests' 		=> 'إدارة النماذج',
            'dublicateTemp'		=>	'هناك نموذج أخر بنفس الأسم',
            'chooseTemp'		=>	'اختر نموذج',
            'txtEditor'			=>	'محرر النص',
            'save'				=>	'حفظ',
            'noTest'            =>    'لا يوجد نموذج بهذا الإسم',
            'editTemp'			=>	'تعديل نموذج',

            //confirm box

            'cancel'		   	=>	'إلغاء',
            'ok'		       	=>	'موافق',
            'confirmTitle'	 	=>	'تأكيد',
            'confirmMsg'		=>	'إضغط موافق للتأكيد أو الغى التنفيذ ',

            //edit patient errs

            'nameErr'      		=>  'name can\'t be empty',
            'nationaIdErr'          =>  'nationl id can\'t be empty',
            'emailErr'         	=>  'email can\'t be empty',
            'phoneErr'         	=>  'phone can\'t be empty',
            'ageErr'          	=>  'age can\'t be empty',

            //settings page

            'settingsTitle'		=>	'الإعدادات',
            'aut-sms'			=>	'إرسال رسالة عند إضافة تقرير جديد',
            'aut-mail'			=>	'إرسال إيميل عند إضافة تقرير جديد',
            'sms-text'			=>	'نص الرسالة الهاتفية',
            'mail-sub'			=>	'عنوان الإيميل',
            'mail-text'			=>	'نص الإيميل',
            'units'                 =>    'عدد الوحدات',

            //search
            'asc'                   =>    'تصاعدى',
            'desc'                  =>    'تنازلى',

            //order
            'select_test_name'      =>    'اختر تحليل',


            




			);

		return $phrases[$phrase];

	}