<?php
      
      
      function lang($phrase)
      {

            $phrases = array(

            //pages titles
            'defaultTitle'       => 'Labortory',
            'ldashboard'         => 'dashboard',
            'Login'                    => 'Login',
            'patientsPage'     => 'Patients',
            'Tests'            => 'Tests',
            //nav bar words
            
            'admin_home'         => 'admin area',
            'cats'                 => 'sections',
            'editProfile'        => 'Edit Profile',
            'settings'           => 'Settings',
            'logout'             =>  'Logout',

            //dashboard
            'members'          => 'members',
            'addMember'        => 'add new member',
            'labs'             => 'labs',
            'dashboard'        => 'dashboard',
            'patients'         => 'Patients',
            'addPatient'       => 'Add Patient',
            'patientList'      => 'Patients List',
            'reports'          => 'Reports',
            'templates'        => 'Tests Templates',
            'addTemp'          => 'Add Template',
            'tempList'         => 'Templates List',

             //patient page words

            'memberType'       =>  'membership type',
            'user'             =>  'user',
            'lab'              =>  'lab',            
            'username'         =>  'username',
            'fullname'         =>  'fullname',
            'pEmail'           =>  'Email',
            'password'         =>  'Password',
            'noDirectAccess'   =>  'you can\'t browse this page directly',
            'pName'            =>   'Name',
            'pPhone'           =>   'Phone',
            'pId'              =>   'National Id',
            'pAge'             =>   'Age',
            'pSex'             =>   'Sex',
            'actions'          =>   'actions',
            'male'                     => 'Male',
            'female'             => 'Female',
            
            //actions
            
            'edit'            =>  'Edit',
            'assign'          =>  'Assign',
            
            //manage results
            'rPname'          => 'Patient Name',
            'rTname'          => 'Test Name',
            'date'            => 'Date',
            'noAuth'          =>    'you are not authorized to access this report',
            'addReport'             =>    'Add Report',

            //manage tests
            'manageTests'           => 'Manage Tests',
            'dublicateTemp'         =>    'there is another template with same name',
            'chooseTemp'            =>    'Choose Template',
            'txtEditor'             =>    'Text Editor',
            'save'                        =>    'Save',
            'noTest'                =>    'no such test name',
            'editTemp'              =>    'Edit Template',


            //confirm box

            'cancel'                =>    'Cancel',
            'ok'                    =>    'Ok',
            'confirmTitle'          =>    'Confirm',
            'confirmMsg'            =>    'Are You Sure?',

            //edit patient errs

            'nameErr'               =>  'name can\'t be empty',
            'nationaIdErr'      =>  'nationl id can\'t be empty',
            'emailErr'              =>  'email can\'t be empty',
            'phoneErr'              =>  'phone can\'t be empty',
            'ageErr'                =>  'age can\'t be empty',

            //settings page

            'settingsTitle'         =>    'Settings',
            'aut-sms'               =>    'Auto send sms on assign new report',
            'aut-mail'              =>    'Auto send e-mail on assign new report',
            'sms-text'              =>    'sms text',
            'mail-sub'              =>    'email subject',
            'mail-text'             =>    'email body',
            'units'             =>    'units',

            //search
            'asc'                   =>    'A to Z',
            'desc'                  =>    'Z to A',


            //order
            'select_test_name'      =>    'select test name',          
            




                  );

            return $phrases[$phrase];

      }