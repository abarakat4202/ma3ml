<?php
			//start manage page
		

		// CHECK IF SEARCH APPLICABLE And store it to get again
		if($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$_SESSION['POST']	=	$_POST;
		}
		else if(!@strpos($_SERVER['HTTP_REFERER'], $_SERVER['PHP_SELF']))
		{
			$_SESSION['POST']	=	'';
		}
			$post 	=	isset($_SESSION['POST']) ? $_SESSION['POST'] : '' ;

			//get tests data from database to show in manage table
		$keyword	=	isset($post['search_string']) ? strtolower($post['search_string']) : '';
		$order  	=	isset($post['order']) ? $post['order'] : 'invoice_timestamp';
		$order_type =	isset($post['order_type']) ? $post['order_type'] : 'Desc';


		if(isset($_REQUEST['patient_id']))
		{
			$stmt	=	$con->prepare(
									  "SELECT * FROM invoices INNER JOIN patients
									   on invoices.l_id = ? 
									   AND  invoices.p_id = ? 
									   AND patients.patient_id = ?

									   AND 
                                          (
                                            patients.patient_name LIKE CONCAT('%',?,'%')
                                            OR invoices.invoice_timestamp LIKE CONCAT('%',?,'%')
                                            )

									   ORDER BY $order $order_type");

			$stmt->execute(array($_SESSION['labid'],$_REQUEST['patient_id'],$_REQUEST['patient_id'],$keyword,$keyword));

			$link	=	$_SERVER['PHP_SELF'].'?'.'patient_id='.$_REQUEST['patient_id'].'&';

			$formAction	=	str_replace('&','',$link);
		}
		else
		{

			$stmt	=	$con->prepare(
									  "SELECT * FROM invoices INNER JOIN patients
									   on invoices.l_id = ? 
									   AND invoices.p_id = patients.patient_id
   									   AND 
                                          (
                                            patients.patient_name LIKE CONCAT('%',?,'%')
                                            OR invoices.invoice_timestamp LIKE CONCAT('%',?,'%')
                                            )
                                            ORDER BY $order $order_type  ");

			$stmt->execute(array($_SESSION['labid'],$keyword,$keyword));

			$link	=	$_SERVER['PHP_SELF'].'?';
			$formAction	=	$_SERVER['PHP_SELF'];

		}


			//fetch data in variable $row
			$rows    =	$stmt->fetchALL();	
			date_default_timezone_set('Africa/Cairo');

			$pgCnt		=	ceil(count($rows)/$pgLen);
			$start		=	(($cPg-1)*$pgLen)+1;
			$end		=	($start+$pgLen-1) >  count($rows) ? count($rows) : ($start+$pgLen-1); 

			$firstPg 	=	$cPg <= floor($paLimit/2) ? 1 : $cPg - floor($paLimit/2) ;
			$lastPg		=	($cPg + floor($paLimit/2)) > $pgCnt ? $pgCnt : ($cPg + floor($paLimit/2));


		 include $tpl.'confirm.php'; ?> 
			<div class="container-fluid">
				<h1 class="text-center"><?php echo lang('reports'); ?></h1>
				<div class="row">
				    <div class="col-lg-12">
				        
				        <div class="panel panel-default">				
							<form action="<?php echo $formAction; ?>" method="POST" accept-charset="utf-8" class="form-inline reset-margin" id="myform">                
								<div class="row">
				                    <div class="col-sm-4">
				                        <div id="dataTables-example_filter" class="dataTables_filter">
				                            <label>
				                                <label for="search_string">Search:</label>
				                                <input type="text" name="search_string" 
				                                value="<?php echo isset($post['search_string']) ? $post['search_string'] : '' ?>" 
				                                class="form-control input-sm" aria-controls="dataTables-example">                            
			                                </label>
				                        </div>
				                    </div>
				                    <div class="col-sm-4">  </div>

				                    <div class="col-sm-4 text-right">
				                        <div class="dataTables_length" id="dataTables-example_length">
				                            <label for="order">Order by:</label> 
				                            <select name="order" aria-controls="dataTables-example" class="form-control input-sm">
												<option value="invoice_timestamp"
												<?php if(@$post['order'] == "invoice_timestamp"){echo "selected";  } ?> 
												><?php echo lang('date'); ?></option>
												<option value="patient_name"
												<?php if(@$post['order'] == "patient_name"){echo "selected";  } ?> 
												><?php echo lang('rPname'); ?></option>
												<option value="test_names"
												<?php if(@$post['order'] == "test_names"){echo "selected";  } ?> 
												><?php echo lang('rTname'); ?></option>

											</select> 
											<select name="order_type" aria-controls="dataTables-example" class="form-control input-sm">
												<option value="Desc"><?php echo lang('desc'); ?></option>
												<option value="Asc" <?php if(@$post['order_type'] == "Asc"){echo "selected";  } ?> 
												><?php echo lang('asc'); ?></option>

											</select> 
											<input type="submit" name="mysubmit" value="Go" class="btn btn-default btn-sm">
				                        </div>
				                    </div>


				                </div>
			                </form>				
							<div class="table-responsive">

								<table class="table table-striped table-bordered table-hover text-center main-table">
									<tr>
										<td><?php echo lang('date'); ?></td>						
										<td><?php echo lang('rPname'); ?></td>
										<td><?php echo lang('rTname'); ?></td>
										<td><?php echo 'amount'; ?></td>
										<td>view</td>
									</tr>

								<?php

								for($i = $start-1; $i<$end; $i++)
								{
									$row = $rows[$i];
									echo 
									'<tr>
										<td>'.date('Y-M-d',strtotime($row["invoice_timestamp"])).'</td>
										<td>'.$row["patient_name"].'</td>
										<td>'.$row["tests_names"].'</td>
										<td>'.$row["total"].'</td>
										<td>
											  <a href="view.php?inv_id='.sha1($row["inovice_id"]).'" class="btn btn-primary " >
											    view 
											  </a>

										</td>
									</tr>';
								}	
								?>
								</table>

							</div>
											
							<nav aria-label="..." class="text-center" 
							<?php echo $pgCnt==1 ? 'style="display:none;' : ""; ?>>
							  <ul class="pagination pagination-lg">
								    <li <?php echo $cPg == 1 ? 'class="disabled"' : ''; ?> >
								    <a href="<?php echo $link.'p='.($cPg == 1 ? 1 : $cPg-1); ?>" aria-label="Previous"><span aria-hidden="true">
								    &laquo;</span></a></li>						  
							  	<?php 

							  		for($p = $firstPg; $p<=$lastPg; $p++)
							  		{

							  			if(isset($_GET['p']) )
							  			{
							  				if($_GET['p'] == $p)
							  				{
							  					$class = 'active';
							  				}
							  				else
							  				{
							  					$class = '';
							  				}
							  			}
							  			else
							  			{
							  				$class = $p == 1 ? 'active' : "" ;
							  			}

							  	?>		
								    <li class="<?php echo $class; ?> "><a href="<?php echo $link.'p='.$p; ?>"><?php echo $p; ?><span class="sr-only">(current)</span></a></li>
							    <?php
							    	}
							    	?>
								    <li <?php echo ($cPg+1)<=$pgCnt ? '' : 'class="disabled"'; ?> >
								      <a href="<?php echo $link.'p='.(($cPg+1)<=$pgCnt ? $cPg+1 : $pgCnt); ?>" aria-label="Next">
								        <span aria-hidden="true">&raquo;</span>
								      </a>
								    </li>	
							  </ul>
							</nav>
						</div>
					</div>
				</div>
			</div>

		<?php

