<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>

<div class='container-fluid'>
	<form class="form-horizontal" action="<?php echo $action ?>" method="POST"> 
		<h1 class="text-center"><?php echo $h1; ?></h1>
		<div class = "col-md-10 text-center">

			
			<div class="row test_container" >
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo lang('pName'); ?></label>
					<div class="col-md-3">
						<select class="form-control items">
							<option><?php echo lang('select_test_name');?></option>;
							<?php foreach ($rows as $key => $row) {
								

								echo '<option value="'.$row['test_price'].'">'.$row['test_name'].'</option>';

								
							}
							?>
						</select>

					</div>
					<div class="col-md-3">
						<input type="date" class="form-control dates">
					</div>
																
		        </div>
	        </div>
	        <div class="row">
		        <div class="form-group">
		        	<div class="pull-right col-md-6">
		        		<a id="delete" class="btn btn-danger">-</a>
		        		<a id="add" class="btn btn-success">+</a>
		        	</div>
		        </div>
	        </div>
			        <script type="text/javascript">
					    $('#items').select2();

					</script>

			
			<div class="row">
				<div class = "form-group">

					<label class="col-sm-2 control-label">Total Amount</label>
					<label class="col-sm-2 col-md-2 control-label text-left " id='amount'>0</label>


				</div>	
			</div>
			<div class="row">
				<div class = "form-group">

				
					<div class = "col-sm-8 col-md-6 col-sm-offset-2">
						<input type="submit" class="btn btn-primary btn-flat" value="<?php echo $btn; ?>"  />

					</div>
				</div>
			</div>	
		</div>
	</form>
</div>	    		

<script type="text/javascript">
	$(function(){
    
    	'use strict';
    	var sum = 0;
    	var tests = [];
		var dates = [];
		var prices= [];    	
    	var changeTriger = function()
						    		{
							    		
							    		$('.items').each( function(i){

									    	 sum = 0;
									    	 tests = [];
											 dates = [];
											 prices= [];    
							    			sum += parseInt($('.items').eq(i).val());					                 
										});
										//show total
							    		$('#amount').html(sum);
							    		

							    		$(".items option:selected").each(function(){
							    			tests.push($(this).text());
							    			prices.push($(this).val());
							    			
							    		});
							    		$(".dates").each(function(){
							    			
							    			dates.push($(this).val());
							    		});
							    		
						    		}
    	var el = $('.test_container').clone();
    	$('.items').change(changeTriger);

    	$('form').submit(function(e)
    	{

    		e.preventDefault();
            $.post('invoices.php?do=assign',{patient_id:"<?php echo $_REQUEST['patient_id'];?>",tests_names:tests,tests_amouts:prices,tests_dates:dates,total:sum},function(data2){
            	

            	location.href = "invoices.php";
            }).then(
            

            )

		});

		$('#add').click(function(){

				
			el.insertAfter( $( ".test_container" ) );
			
	    	$('.items').change(changeTriger);
		});
		$('#delete').click(function(){

				
			$( ".test_container" ).last().remove();
			
			
		});

    });
</script>