<?php 
		
		$sex = isset($sex) ? $sex : "" ;
?>
<div class='container-fluid'>
	<form class="form-horizontal" action="<?php echo $action ?>" method="POST"> 
		<h1 class="text-center"><?php echo $h1; ?></h1>
		<div class = "form-group">

			<label class="col-sm-2 control-label"><?php echo lang('pName'); ?></label>
			<div class = "col-sm-8 col-md-6">
				<input type="text" class="form-control" name="patientname"
				 value="<?php echo isset($patientname) ? $patientname : "" ;?> " required="" />

			</div>
		</div>

		<div class = "form-group">

			<label class="col-sm-2 control-label"><?php echo lang('pId'); ?></label>
			<div class = "col-sm-8 col-md-6">
				<input type="number" class="form-control" name="patientnational"
				 value="<?php echo isset($nationalid) ? intval($nationalid) : "" ;?>" />

			</div>
		</div>

		<div class = "form-group">

			<label class="col-sm-2 control-label"><?php echo lang('pPhone'); ?></label>
			<div class = "col-sm-8 col-md-6">
				<input type="number" class="form-control" name="patientphone"
				 value="<?php echo isset($patientphone) ? $patientphone : "" ;?>" required="" />

			</div>
		</div>

		<div class = "form-group">

			<label class="col-sm-2 control-label"><?php echo lang('pEmail'); ?></label>
			<div class = "col-sm-8 col-md-6">
				<input type="email" class="form-control" name="patientemail"
				 value="<?php echo isset($patientemail) ? $patientemail : "" ;?>" />

			</div>
		</div>	

		<div class = "form-group">

			<label class="col-sm-2 control-label">Referer</label>
			<div class = "col-sm-8 col-md-6">
				<input type="text" class="form-control" name="referer"
				 value="<?php echo isset($referer) ? $referer : "" ;?>" />

			</div>
		</div>

		<div class = "form-group">

			<label class="col-sm-2 control-label"><?php echo lang('pAge'); ?></label>
			<div class = "col-sm-8 col-md-6">
				<input type="number" class="form-control" name="patientage"
				 value="<?php echo isset($patientage) ? $patientage : "" ;?>" required="" />

			</div>
		</div>

		<div class = "form-group">

			<label class="col-sm-2 control-label"><?php echo lang('pSex'); ?></label>
			<div class = "col-sm-8 col-md-6">
                
                <label class="radio-inline">
                    <input class="report_type" type="radio"
                    	<?php echo $sex != "female" ? "checked" : "" ;?>
                      value="male" id="sex" name="sex"> <?php echo lang('male'); ?>
                </label>
                <label class="radio-inline">
                    <input class="report_type" type="radio"
                    	<?php echo $sex == "female" ? "checked" : "" ;?>
                     value="female" id="sex" name="sex"> <?php echo lang('female'); ?>
                </label>
                        

			</div>
		</div>



		<div class = "form-group">

			
			<div class = "col-sm-8 col-md-6 col-sm-offset-2">
				<input type="submit" class="btn btn-primary btn-flat" value="<?php echo $btn; ?>"  />

			</div>
		</div>	

	</form>
</div>
