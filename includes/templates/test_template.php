<div class="container-fluid">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?php echo $h1; ?></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
                <div class="panel panel-default">
            <div class="panel-heading">

            </div>


            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-10">
						<form action="<?php echo $action; ?>" method="POST" accept-charset="utf-8" role="form" id="" enctype="multipart/form-data">
                        <div class="form-group">
                            <label><?php echo lang('pName'); ?></label>
                            <input class="form-control" name="name" id="fullname" 
                            value="<?php echo isset($Name) ? $Name : ""; ?>"
                            <?php echo isset($Name) ? 'readonly=""' : ""; ?> type="text" autocomplete="off" >
                        </div>
                        
                        <div class="form-group" <?php echo isset($tests) ? "" : 'style="display:none;"'; ?> >
                            <label><?php echo lang('chooseTemp'); ?></label>
                            <select class="form-control" name="test" id="test">
                                <?php echo isset($tests) ? $tests : ""; ?>
                            </select>
                        </div>

                        <div class="form-group <?php echo isset($_REQUEST['patient_id']) ? 'hidden' : ""; ?>">
                            <label>Price</label>
                            <input class="form-control" name="price" id="price" 
                            value="<?php echo isset($price) ? $price : ""; ?>"
                            type="number" autocomplete="off" >
                        </div>


                        <div class="form-group" id="text_field">
                            <label><?php echo lang('txtEditor'); ?></label>
				            <textarea name="editor1" id="editor1" rows="10" cols="80">
				                <?php echo isset($testVal) ? $testVal : ""; ?>

				            </textarea>
				        <!--    <script src="includes/libs/ckeditor/ckeditor.js"></script>
				            <script>
				                // Replace the <textarea id="editor1"> with a CKEditor
				                // instance, using default configuration.
									CKEDITOR.replace( 'editor1', {
									    customConfig: '/lab/includes/libs/ckeditor/custom/config.js'
									});

				            </script>
                        -->
                        </div>

                        <div class="form-group">                            
                            <button class="btn btn-default btn-primary" type="submit"><?php echo $btn; ?></button>

                        </div>    
						</form>                    
						</div>
                    <!-- /.col-lg-6 (nested) -->

                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
</div>
<script>
$(function(){
    'use strict';
    tinymce.init({
        selector:"#editor1",
        branding: false,
        color_picker_callback: function(callback, value) 
        {
            callback('#FF00FF');
        },

        plugins: "paste lists",
        toolbar: "undo redo | alignleft aligncenter alignright alignjustify | bold italic underline | bullist numlist",
        menubar: false,
        browser_spellcheck: true,
        statusbar: false,
        fix_list_elements: true,
        content_style: "body.mce-content-body{ font-size: 13px; } p{ margin-top: 0.5em; margin-bottom: 0.5em; }",
        element_format : "html",
        valid_elements: "p,br,strong,bold,b,i,em,u,ul,ol,li,span[style]",
        valid_styles: {
            "span": "text-decoration"
        },
        schema: "html5",
        init_instance_callback: function(editor){
            editor.on("change", function(){
                editor.save();
                $(editor.getElement())
                .text(editor.getContent())
                .trigger("blur");

            });
            editor.on("blur", function(){
                editor.save();
                $(editor.getElement())
                .text(editor.getContent())
                .trigger("blur");
            });
        }
    });
    $(".mce-branding-powered-by").hide();
})    
</script>
