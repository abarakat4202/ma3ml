
<!-- LABS SIDEBAR -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="ldashboard.php"><?php echo $_SESSION['labname'] ?></a>
            </div>
            <!-- Top Menu Items -->
            <!--<ul class="nav navbar-nav">
                <li class="active"><a href="#">sections<span class="sr-only">(current)</span></a></li>

            </ul>-->
            <ul class="nav 
            <?php if($lang == 'english'){echo 'navbar-right';}else{echo 'navbar-left';} ?> top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <?php 

                        if($lang == 'arabic')
                        {
                        ?>    
                          
                            <img src="<?php echo $img; ?>AR.gif">
                            عربى
                        <?php
                        }
                        else
                        {?>
                          
                            <img src="<?php echo $img; ?>EN.gif">
                            english
                        <?php
                        }
                        ?>
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>

                            <a href="<?php echo $func; ?>lang.php?lang=english">
                            <img src="<?php echo $img; ?>EN.gif">
                            english</a>
                        </li>
                        <li>

                            <a href="<?php echo $func; ?>lang.php?lang=arabic">
                            <img src="<?php echo $img; ?>AR.gif">
                            عربى</a>
                        </li>                        
                    </ul>
                </li>
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['labname'] ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li style="display:none;">
                            <a href="labs.php?do=edit"><i class="fa fa-fw fa-user"></i><?php echo lang('editProfile'); ?></a>
                        </li>
                        <li>
                            <a href="" style="color: #f39c12;"><i class="fa fa-fw fa-refresh"></i><?php echo lang('units'); ?> : 
                            <?php echo chkExist('credit','labs','lab_id = '.$_SESSION['labid'])[1][0]['credit']; ?></a>
                        </li>                        
                        <li>
                            <a href="settings.php"><i class="fa fa-fw fa-gear"></i><?php echo lang('settings'); ?></a>
                        </li>
                        <li>
                            <a href="mainTemp.php"><i class="fa fa-fw fa-gear"></i>templates settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i><?php echo lang('logout'); ?></a>
                        </li>
                    </ul>
                </li>

            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li >
                        <a href="ldashboard.php"><i class="fa fa-fw fa-dashboard"></i><?php echo lang('dashboard'); ?></a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-hospital-o"></i><?php echo lang('patients'); ?><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="patients.php?do=add"><?php echo lang('addPatient'); ?></a>
                            </li>
                            <li>
                                <a href="patients.php"><?php echo lang('patientList'); ?></a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="invoices.php" ><i class="fa fa-fw fa-users"></i><?php echo 'invoices'; ?></a>

                    </li>

                    <li>
                        <a href="results.php" ><i class="fa fa-fw fa-users"></i><?php echo lang('reports'); ?></a>

                    </li>

                    <li >
                        <a href="javascript:;" data-toggle="collapse" data-target="#Tests-Templates"><i class="fa fa-fw fa-users"></i><?php echo lang('templates'); ?><i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="Tests-Templates" class="collapse">
                            <li>
                                <a href="tests.php?do=add"><?php echo lang('addTemp');?></a>
                            </li>
                            <li>
                                <a href="tests.php"><?php echo lang('tempList'); ?></a>
                            </li>
                        </ul>
                    </li>                                         
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        
    