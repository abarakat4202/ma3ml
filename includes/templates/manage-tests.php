<?php
			//start manage page
			// CHECK IF SEARCH APPLICABLE And store it to get again
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$_SESSION['POST']	=	$_POST;
			}
			else if(!@strpos($_SERVER['HTTP_REFERER'], $_SERVER['PHP_SELF']))
			{
				$_SESSION['POST']	=	'';
			}
				$post 	=	isset($_SESSION['POST']) ? $_SESSION['POST'] : '' ;

				
			$keyword	=	isset($post['search_string']) ? strtolower($post['search_string']) : '';


			//get tests data from database to show in manage table
			$stmt	=	$con->prepare(
									  "SELECT * FROM tests_templates
									   WHERE lab_ids = ?
									   AND  test_name LIKE CONCAT('%',?,'%')");
			$stmt->execute(array($_SESSION['labid'],$keyword));


			//fetch data in variable $row
			$rows    =	$stmt->fetchALL();

			$pgCnt		=	ceil(count($rows)/$pgLen);
			$start		=	(($cPg-1)*$pgLen)+1;
			$end		=	($start+$pgLen-1) >  count($rows) ? count($rows) : ($start+$pgLen-1); 

			$firstPg 	=	$cPg <= floor($paLimit/2) ? 1 : $cPg - floor($paLimit/2) ;
			$lastPg		=	($cPg + floor($paLimit/2)) > $pgCnt ? $pgCnt : ($cPg + floor($paLimit/2));				
		?>
			
			<div class="container-fluid">

				<h1 class="text-center"><?php echo lang('manageTests'); ?> </h1>
				<div class="row">
				    <div class="col-lg-12">
				        
				        <div class="panel panel-default">				
							<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" accept-charset="utf-8" class="form-inline reset-margin" id="myform">                
								<div class="row">
				                    <div class="col-sm-4">
				                        <div id="dataTables-example_filter" class="dataTables_filter">
				                            <label>
				                                <label for="search_string">Search:</label>
				                                <input type="text" name="search_string" 
				                                value="<?php echo isset($post['search_string']) ? $post['search_string'] : '' ?>" 
				                                class="form-control input-sm" aria-controls="dataTables-example">

											<input type="submit" name="mysubmit" value="Go" class="btn btn-default btn-sm">	                                                            
			                                </label>
				                        </div>

				                        </div>
				                    </div>


				               
			                </form>				
							<div class="table-responsive">

								<table class="table table-striped table-bordered table-hover text-center main-table">
									<tr>
										<td><?php echo lang('rTname'); ?></td>
										<td><?php echo lang('actions'); ?></td>
									</tr>

								<?php

								foreach($rows as $row)
								{
									echo 
									'<tr>
										<td>'.$row["test_name"].'</td>
										<td>
											<div class="btn-group">
											  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											    Action <span class="caret"></span>
											  </button>
											  <ul class="dropdown-menu">
											    <li><a href="tests.php?do=edit&test='.$row["test_name"].'">edit</a></li>
											  </ul>
											</div>


										</td>
									</tr>';
								}	
								?>
								</table>
								
							</div>
								<nav aria-label="..." class="text-center" 
								<?php echo $pgCnt==1 ? 'style="display:none;' : ""; ?>>
									  <ul class="pagination pagination-lg">
										    <li <?php echo $cPg == 1 ? 'class="disabled"' : ''; ?> >
										    <a href="?p=<?php echo $cPg == 1 ? 1 : $cPg-1; ?>" aria-label="Previous"><span aria-hidden="true">
										    &laquo;</span></a></li>						  
									  	<?php 

									  		for($p = $firstPg; $p<=$lastPg; $p++)
									  		{

									  			if(isset($_GET['p']) )
									  			{
									  				if($_GET['p'] == $p)
									  				{
									  					$class = 'active';
									  				}
									  				else
									  				{
									  					$class = '';
									  				}
									  			}
									  			else
									  			{
									  				$class = $p == 1 ? 'active' : "" ;
									  			}

									  	?>		
										    <li class="<?php echo $class; ?> "><a href="?p=<?php echo $p; ?>"><?php echo $p; ?><span class="sr-only">(current)</span></a></li>
									    <?php
									    	}
									    	?>
										    <li <?php echo ($cPg+1)<=$pgCnt ? '' : 'class="disabled"'; ?> >
										      <a href="?p=<?php echo ($cPg+1)<=$pgCnt ? $cPg+1 : $pgCnt; ?>" aria-label="Next">
										        <span aria-hidden="true">&raquo;</span>
										      </a>
										    </li>	
									  </ul>
							</nav>
						</div>
					</div>
				</div>
			</div>							
	