	<?php 
		$css	=	'css/';				   //css directory

	?>
<link href="css/bootstrap.min.css" rel="stylesheet" media="all">


	<style type="text/css" >

body {
    background-color: #efefef;
}
#printableArea{
    margin: 10px auto;
    padding: 5px;
    max-width: 850px;
    background-color: #fff;
    border: 1px solid #ccc;
    -moz-border-radius: 6px;
    -webkit-border-radius: 6px;
    -o-border-radius: 6px;
    border-radius: 6px;
}
* {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
}
.page {
    width: 21cm;
    min-height: 29.7cm;
    padding: 2cm;
    margin: 1cm auto;
    border: 1px #D3D3D3 solid;
    border-radius: 5px;
    background: white;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}
.subpage {
    padding: 1cm;
    border: 5px red solid;
    height: 256mm;
    outline: 2cm #FFEAEA solid;
}

@page {
    size: A4;
    margin: 0;
}
@media print {
    .page {
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
    }
}
	.demo-droppable img,.demo-droppable2 img
	  {
	  	max-width: 100%;
	    max-height: 100%;
	  }
	  .head
	  {
	   	/*padding: 10px 10px;*/
		overflow: hidden;
		height: 100%;
	  }
	  .h_center
	  {
	  	position: relative;
		padding: 10px 10px;
	  }
	    .patient_info
	  {
	  	border-bottom:1px solid grey;
  		border-top:1px solid grey;

	  }
	    .temp_body
	  {
	  	height: 400px;
	  	/*border-bottom:1px solid grey;*/
	  }
	  .footer
	  {
	  	height:150px;
	  }
	  .invoice
	  {
	  	    font-size: large;
	  }
	  .invoice-details
	  {
	  	border-bottom: 1px solid #ddd;
	  	margin-top:10px;    
	  	margin-bottom:10px;    
	  	line-height: 40px;
	  }
  	</style>
	<div class='container-fluid'>
		<div class="col-xs-10 col-xs-offset-1">
			<div class="row header">
				<div class="col-xs-3 head">

					<div class="demo-droppable head">
						<?php echo $h_left== '' ? "<p>+</p>" : "<img src=\"$h_left\">" ?>
					  	
					</div>

				</div>
				<div class="col-xs-6 head-center">

					<div class="h_center" id="h_center"><?php echo $h_center; ?></div>

				</div>
				<div class="col-xs-3 head">

					<div class="demo-droppable2 head">
						<?php echo $h_right== '' ? "<p>+</p>" : "<img src=\"$h_right\">" ?>
					</div>

				</div>
			</div>
			<div class="row patient_info text-center">
				Date : <?php echo $reportDate;?><br>
				Name : <?php echo $reportName;?><br>
				Age : <?php echo $reportAge;?><br>
				Sex : <?php echo $reportSex;?>
				<?php echo $reportReferer != '' ? "<br> Referer : $reportReferer" : '' ;?>
			</div>
			<div class="row temp_body">
			<?php echo $body; ?>
			</div>
			<div class="row footer">
				<div class="col-xs-6 footer-left">
					<div class="f_left" id="f_left"><?php echo $f_left; ?></div>
				</div>
				<div class="col-xs-6 footer-right">
					<div class="f_right" id="f_right"><?php echo $f_right; ?></div>
				</div>
			</div>
		</div>
	</div>
