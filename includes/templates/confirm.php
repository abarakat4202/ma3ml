			    <link rel="stylesheet" href="<?php echo $css;?>/confirm.css" />
   				 <script src="<?php echo $js;?>confirm.js"></script>

    <div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo lang('confirmTitle'); ?></h4>
                </div>
                <div class="modal-body">
                    <p><?php echo lang('confirmMsg'); ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo lang('cancel'); ?></button>
                    <button type="button" class="btn btn-primary btn-ok"><?php echo lang('ok'); ?></button>
                </div>
            </div>
        </div>
    </div>