<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" >
		<title ><?php echo isset($pageTitle) ? $pageTitle : lang('defaultTitle'); ?></title>
		<link rel="stylesheet" href="<?php echo $css; ?>bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo $css; ?>sb-admin.css">
		<link rel="stylesheet" href="<?php echo $css; ?>plugins/morris.css">

		<?php 

			if($lang == 'arabic')
			{
				?>
					<link rel="stylesheet" href="<?php echo $css; ?>bootstrap-rtl.min.css">
					<link rel="stylesheet" href="<?php echo $css; ?>sb-admin-rtl.css">
					<link rel="stylesheet" href="<?php echo $css; ?>plugins/morris.css">


				<?php

			}
			?>
		
        <link rel="stylesheet" href="<?php echo $css; ?>font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $css; ?>front-end.css">
        <link rel="stylesheet" href="<?php echo $css; ?>toggle.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Raleway:400,700,900" rel="stylesheet"> 
        <script type="text/javascript" src="<?php echo $js; ?>jquery.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]--> 		

	</head>
	<body>

		<div id="wrapper">


	