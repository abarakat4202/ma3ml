<?php
			//start manage page
			// CHECK IF SEARCH APPLICABLE And store it to get again
			if($_SERVER['REQUEST_METHOD'] == 'POST')
			{
				$_SESSION['POST']	=	$_POST;
			}
			else if(!@strpos($_SERVER['HTTP_REFERER'], $_SERVER['PHP_SELF']))
			{
				$_SESSION['POST']	=	'';
			}
				$post 	=	isset($_SESSION['POST']) ? $_SESSION['POST'] : '' ;

				//handle insert user redirect


				//get tests data from database to show in manage table
			$keyword	=	isset($post['search_string']) ? strtolower($post['search_string']) : '';
			$order  	=	isset($post['order']) ? $post['order'] : 'relation_start_date';
			$order_type =	isset($post['order_type']) ? $post['order_type'] : 'DESC';

			//get patients data from database to show in manage table

			$stmt	=	$con->prepare(
									  "SELECT * FROM labs_patients_relation_table
									   INNER JOIN patients
									    ON labs_patients_relation_table.patient_ids = patients.patient_id  
									    AND labs_patients_relation_table.lab_ids = ?
									    AND 
                                          (
                                            patients.patient_name LIKE CONCAT('%',?,'%')
                                            OR patients.patient_phone LIKE CONCAT('%',?,'%')
                                            OR patients.patient_national_id LIKE CONCAT('%',?,'%')
                                            OR patients.email LIKE CONCAT('%',?,'%')
                                            OR patients.referer LIKE CONCAT('%',?,'%')
                                            )
                                        ORDER BY $order $order_type");
			$stmt->execute(array($_SESSION['labid'],$keyword,$keyword,$keyword,$keyword,$keyword));


			//fetch data in variable $row
			$rows    =	$stmt->fetchALL();
			$pgCnt		=	ceil(count($rows)/$pgLen);
			$start		=	(($cPg-1)*$pgLen)+1;
			$end		=	($start+$pgLen-1) >  count($rows) ? count($rows) : ($start+$pgLen-1); 

			$firstPg 	=	$cPg <= floor($paLimit/2) ? 1 : $cPg - floor($paLimit/2) ;
			$lastPg		=	($cPg + floor($paLimit/2)) > $pgCnt ? $pgCnt : ($cPg + floor($paLimit/2));	
		?>
			
			<div class="container-fluid">

				<h1 class="text-center">Manage Patients</h1>
				<div class="row">
				    <div class="col-lg-12">
				        
				        <div class="panel panel-default">				
							<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" accept-charset="utf-8" class="form-inline reset-margin" id="myform">                
								<div class="row">
				                    <div class="col-sm-4">
				                        <div id="dataTables-example_filter" class="dataTables_filter">
				                            <label>
				                                <label for="search_string">Search:</label>
				                                <input type="text" name="search_string" 
				                                value="<?php echo isset($post['search_string']) ? $post['search_string'] : '' ?>" 
				                                class="form-control input-sm" aria-controls="dataTables-example">                            
			                                </label>
				                        </div>
				                    </div>
				                    <div class="col-sm-4">  </div>

				                    <div class="col-sm-4 text-right">
				                        <div class="dataTables_length" id="dataTables-example_length">
				                            <label for="order">Order by:</label> 
				                            <select name="order" aria-controls="dataTables-example" class="form-control input-sm">
												<option value="relation_start_date"
												<?php if(@$post['order'] == "relation_start_date"){echo "selected";  } ?> 
												><?php echo 'Date'; ?></option>
												<option value="patient_name"
												<?php if(@$post['order'] == "patient_name"){echo "selected";  } ?> 
												><?php echo lang('pName'); ?></option>
												<option value="patient_phone"
												<?php if(@$post['order'] == "patient_phone"){echo "selected";  } ?> 
												><?php echo lang('pPhone'); ?></option>
												<option value="email"
												<?php if(@$post['order'] == "email"){echo "selected";  } ?> 
												><?php echo lang('pEmail'); ?></option>
												<option value="referer"
												<?php if(@$post['order'] == "referer"){echo "selected";  } ?> 
												>Referer</option>
												<option value="patient_national_id"
												<?php if(@$post['order'] == "patient_national_id"){echo "selected";  } ?> 
												><?php echo lang('pId'); ?></option>
												<option value="patient_Age"
												<?php if(@$post['order'] == "patient_Age"){echo "selected";  } ?> 
												><?php echo lang('pAge'); ?></option>
												<option value="patient_Sex"
												<?php if(@$post['order'] == "patient_Sex"){echo "selected";  } ?> 
												><?php echo lang('pSex'); ?></option>
											</select> 
											<select name="order_type" aria-controls="dataTables-example" class="form-control input-sm">
												<option value="Asc"><?php echo lang('asc'); ?></option>
												<option value="Desc" 
												<?php if(@$post['order_type'] == "Desc"){echo "selected";  } ?> 
												><?php echo lang('desc'); ?></option>
											</select> 
											<input type="submit" name="mysubmit" value="Go" class="btn btn-default btn-sm">
				                        </div>
				                    </div>


				                </div>
			                </form>
								<div class="table-responsive">

									<table class="table table-striped table-bordered table-hover text-center main-table">
										<tr>
											<td><?php echo 'Date'; ?></td>
											<td><?php echo lang('pName'); ?></td>
											<td><?php echo lang('pPhone'); ?></td>
											<td><?php echo lang('pEmail'); ?></td>
											<td>Referer</td>
											<td><?php echo lang('pId'); ?></td>
											<td><?php echo lang('pAge'); ?></td>
											<td><?php echo lang('pSex'); ?></td>
											<td><?php echo lang('actions'); ?></td>
										</tr>

									<?php

									foreach($rows as $row)
									{
										echo 
										'<tr>
											<td>'.$row["relation_start_date"].'</td>
											<td>'.$row["patient_name"].'</td>
											<td>'.$row["patient_phone"].'</td>
											<td>'.$row["email"].'</td>
											<td>'.$row["referer"].'</td>
											<td>'.$row["patient_national_id"].'</td>
											<td>'.$row["patient_Age"].'</td>
											<td>'.$row["patient_Sex"].'</td>
											<td \'class="text-left"\'>
												<div class="btn-group">
												  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.lang('actions').'<span class="caret"></span>
												  </button>
												  <ul class="dropdown-menu">
												    <li><a href="patients.php?do=edit&patient_id='.$row["patient_id"].'">'.lang('edit').'</a></li>
												    <li><a href="invoices.php?do=order&patient_id='.$row["patient_id"].'">'.'order'.'</a></li>
												    <li><a href="results.php?do=add&patient_id='.$row["patient_id"].'">'.lang('assign').'</a></li>
												  </ul>
												</div>


											</td>
										</tr>';
									}	
									?>
									</table>
									
								</div>
									<nav aria-label="..." class="text-center" 
									<?php echo $pgCnt==1 ? 'style="display:none;' : ""; ?>>
									  <ul class="pagination pagination-lg">
										    <li <?php echo $cPg == 1 ? 'class="disabled"' : ''; ?> >
										    <a href="?p=<?php echo $cPg == 1 ? 1 : $cPg-1; ?>" aria-label="Previous"><span aria-hidden="true">
										    &laquo;</span></a></li>						  
									  	<?php 

									  		for($p = $firstPg; $p<=$lastPg; $p++)
									  		{

									  			if(isset($_GET['p']) )
									  			{
									  				if($_GET['p'] == $p)
									  				{
									  					$class = 'active';
									  				}
									  				else
									  				{
									  					$class = '';
									  				}
									  			}
									  			else
									  			{
									  				$class = $p == 1 ? 'active' : "" ;
									  			}

									  	?>		
										    <li class="<?php echo $class; ?> "><a href="?p=<?php echo $p; ?>"><?php echo $p; ?><span class="sr-only">(current)</span></a></li>
									    <?php
									    	}
									    	?>
										    <li <?php echo ($cPg+1)<=$pgCnt ? '' : 'class="disabled"'; ?> >
										      <a href="?p=<?php echo ($cPg+1)<=$pgCnt ? $cPg+1 : $pgCnt; ?>" aria-label="Next">
										        <span aria-hidden="true">&raquo;</span>
										      </a>
										    </li>	
									  </ul>
									</nav>
						</div>
					</div>
				</div>
			</div>								
		