<?php


session_start();

if(!isset($_SESSION['labname']))
{
	header('location:index.php');
	exit();
}

else
{
	$do 		=	isset($_REQUEST['do']) ? $_REQUEST['do'] : 'manage';

	$lsidebar	=	'yes';
	$pageTitle	=	'patientsPage';

	include 'init.php';

	//
	switch ($do) {
		case 'manage':

					include $tpl.'manage-patients.php';
			break;


		case 'add':
					$h1		=	'Add Patient';
					$btn	=	'Add';
					$action	=	'patients.php?do=insert';
					include $tpl.'patients-add-edit.php';
			break;

		case 'insert':
						$h1		=	'Add Patient';
						if($_SERVER['REQUEST_METHOD'] !== 'POST')
						{
							redirectFun('you can\'t browse this page directly',$h1);
						}
						else
						{
							//an array to push errs on it
							$errors	=	array();

							//errors chek
							if(empty($_REQUEST['patientname'] ))
							{
								array_push($errors, lang('nameErr'));
							}

							/*if(empty($_REQUEST['patientnational'] ))
							{
								array_push($errors, lang('nationaIdErr'));
							}*/

							if(empty($_REQUEST['patientphone'] ))
							{
								array_push($errors, lang('phoneErr'));
							}

							/*if(empty($_REQUEST['patientemail'] ))
							{
								array_push($errors, lang('emailErr'));
							}*/

							if(empty($_REQUEST['patientage'] ))
							{
								array_push($errors, lang('ageErr'));
							}


							//iferrors
							if(count($errors))
							{


								$h1		=	'Add Patient';
								echo '
								<div class="container-fluid">
								<h1 class="text-center">'.$h1.'</h1>
									<div class="row">';


								foreach($errors as $key => $err)
								{

									echo	'<div class="col-md-6 col-md-offset-3" style="margin-top:5px;">
												<div class="alert alert-danger text-center" role="alert">';								
									echo $err;

									echo			'</div>
											</div>';
							
								}

								echo '</div></div>' ;


							}
							//if no error
							else 
							{
								//check if patient has been added before to the database
								if(chkExist("*",'patients','patient_phone = ' . $_REQUEST['patientphone'])[0] > 0)
								{
									//patient has been added before to the database
									$patient_id =chkExist("*",'patients','patient_phone = ' . $_REQUEST['patientphone'])[1][0]['patient_id'];

									

									//now we have to check if he has been added before to this lab
									if(chkExist("*",'labs_patients_relation_table',
										'patient_ids	 = ' . $patient_id . ' AND lab_ids	= ' . $_SESSION['labid'] )[0] > 0)
									{
										//patient has been added before to this lab
										errWh('the patient with this phone has been added before to your lab',$h1="Add Patient");
									}
									else
									{
										$stmt	=	$con->prepare("INSERT INTO labs_patients_relation_table
																	(lab_ids,patient_ids)
																	VALUES(?,?)");
										$stmt	-> execute(array($_SESSION['labid'],$patient_id));
										scs('your updates has been saved',$h1);

									}
								}
								//if the patient hasn't been added before;
								else
								{
									/////////////////////////
									//add to patients table//
									/////////////////////////

										$stmt	=	$con->prepare("INSERT INTO patients
																	(patient_name,patient_phone,email,referer,patient_Age,patient_Sex,patient_national_id)
																	VALUES(?,?,?,?,?,?,?)");
										$stmt	-> execute(array(
											$_REQUEST['patientname'],
											$_REQUEST['patientphone'],
											$_REQUEST['patientemail'],
											$_REQUEST['referer'],
											$_REQUEST['patientage'],
											$_REQUEST['sex'],
											$_REQUEST['patientnational'] == '' ? 0 : $_REQUEST['patientnational'],
											));

									//////////////////////////////////////////////
									//make realtion between patient and the lab//
									/////////////////////////////////////////////

									$patient_id =chkExist("*",'patients','patient_phone = ' . $_REQUEST['patientphone'])[1][0]['patient_id'];

										$stmt	=	$con->prepare("INSERT INTO labs_patients_relation_table
																	(lab_ids,patient_ids)
																	VALUES(?,?)");
										$stmt	-> execute(array($_SESSION['labid'],$patient_id));

										//success message
										scs('your updates has been saved test',$h1);
										$_SESSION['search_string'] = $_REQUEST['patientphone'];
										echo ' <meta http-equiv="Refresh" content="2; url=patients.php">';
	
								}

							}							
						}
						
			break;

		case 'edit':
					$h1				=	'Edit Patient Data';

				//check if 	comes with valid patient id and under this lab proberty 
					if(isset($_REQUEST['patient_id']) &&
					 chkExist("*",'labs_patients_relation_table',
						'patient_ids	 = ' . $_REQUEST['patient_id'] . ' AND lab_ids	= ' . $_SESSION['labid'] )[0] > 0)
					{

					$btn			=	'Save';
					$action			=	'patients.php?do=update';

					$arr 			=	chkExist("*","patients","patient_id = " . $_REQUEST['patient_id'])[1][0];
					$patientname 	=	$arr['patient_name'];
					$nationalid		=	$arr['patient_national_id'];
					$patientphone	=	$arr['patient_phone'];
					$patientemail	=	$arr['email'];
					$referer		=	$arr['referer'];
					$patientage		=	$arr['patient_Age'];
					$sex			=	$arr['patient_Sex'];
					$action			=	'patients.php?do=update&patient_id='.$_REQUEST['patient_id'];

					include $tpl.'patients-add-edit.php';

					}
					else
					{
						errWh('no valid patient selected',$h1="Edit Patient Data");
					}	
			break;			

		case 'update':
						$h1		=	'Edit Patient Data';
						if($_SERVER['REQUEST_METHOD'] !== 'POST')
						{
							redirectFun('you can\'t browse this page directly',$h1);
						}
						else
						{
							//an array to push errs on it
							$errors	=	array();

							//errors chek
							if(empty($_REQUEST['patientname'] ))
							{
								array_push($errors, lang('nameErr'));
							}
							/*
							if(empty($_REQUEST['patientnational'] ))
							{
								array_push($errors, lang('nationaIdErr'));
							}
							*/
							if(empty($_REQUEST['patientphone'] ))
							{
								array_push($errors, lang('phoneErr'));
							}

							if(empty($_REQUEST['patientemail'] ))
							{
								array_push($errors, lang('emailErr'));
							}

							if(empty($_REQUEST['patientage'] ))
							{
								array_push($errors, lang('ageErr'));
							}


							//iferrors
							if(count($errors))
							{
								$h1		=	'Edit Patient Data';
								echo '
								<div class="container-fluid">
								<h1 class="text-center">'.$h1.'</h1>
									<div class="row">';


								foreach($errors as $key => $err)
								{

									echo	'<div class="col-md-6 col-md-offset-3" style="margin-top:5px;">
												<div class="alert alert-danger text-center" role="alert">';								
									echo $err;

									echo			'</div>
											</div>';
							
								}

								echo '</div></div>' ;


							}
							//if no error
							else 
							{

								
									/////////////////////////
									//edit to patients table//
									/////////////////////////

										$stmt	=	$con->prepare("UPDATE patients SET 
																		patient_name = ?,
																		patient_phone= ?,
																		email = ?,
																		referer = ?,
																		patient_Age = ?,
																		patient_sex = ?,
																		patient_national_id = ?

																		WHERE patient_id = ?");



										$stmt	-> execute(array(
											$_REQUEST['patientname'],
											$_REQUEST['patientphone'],
											$_REQUEST['patientemail'],
											$_REQUEST['referer'],
											$_REQUEST['patientage'],
											$_REQUEST['sex'],
											$_REQUEST['patientnational'],
											$_REQUEST['patient_id'],
											));


							echo("<script>location.href = 'patients.php';</script>");


							}							
						}
						
			break;

		


		default:
		;
			break;
	}



	//footer
	include $tpl.'footer.php';	
}